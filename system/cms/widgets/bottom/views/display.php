<ul class="square4">
	<li class="food">
		<div class="detail-wp"><a href="<?php echo $link_catering; ?>" class="view-detail">View Detail</a></div>
		<div class="icon"></div>
		<div class="icon-mobile"></div>
		<div class="title"><?php echo $title_catering; ?></div>
		<div class="text"><?php echo $introtext_catering; ?></div>
	</li>
	<li class="delivery">
		<div class="detail-wp"><a href="<?php echo $link_delivery; ?>" class="view-detail">View Detail</a></div>
		<div class="icon"></div>
		<div class="icon-mobile"></div>
		<div class="title"><?php echo $title_delivery; ?></div>
		<div class="text"><?php echo $introtext_delivery; ?></div>
	</li>
	<li class="menu">
		<div class="detail-wp"><a href="<?php echo $link_menu; ?>" class="view-detail">View Detail</a></div>
		<div class="icon"></div>
		<div class="icon-mobile"></div>
		<div class="title"><?php echo $title_menu; ?></div>
		<div class="text"><?php echo $introtext_menu; ?></div>
	</li>
	<li class="place">
		<div class="detail-wp"><a href="<?php echo $link_retaurant; ?>" class="view-detail">View Detail</a></div>
		<div class="icon"></div>
		<div class="icon-mobile"></div>
		<div class="title"><?php echo $title_retaurant; ?></div>
		<div class="text"><?php echo $introtext_retaurant; ?></div>
	</li>
	<div class="clear"></div>
</ul>