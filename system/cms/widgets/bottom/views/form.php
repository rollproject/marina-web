<ul>
	<label><b>Catering Widget</b></label>
	<li class="even">
		<label>Title</label>
		<?php echo form_input(array('name'=>'title_catering', 'value' => $options['title_catering'])); ?>
	</li>
	<li class="even">
		<label>Introtext</label>
		<?php echo form_textarea(array('name'=>'introtext_catering', 'value' => $options['introtext_catering'])); ?>
	</li>
	<li class="even">
		<label>Link</label>
		<?php echo form_input(array('name'=>'link_catering', 'value' => $options['link_catering'])); ?>
	</li>
</ul>
<br />
<ul>
	<label><b>Delivery Service</b></label>
	<li class="even">
		<label>Title</label>
		<?php echo form_input(array('name'=>'title_delivery', 'value' => $options['title_delivery'])); ?>
	</li>
	<li class="even">
		<label>Introtext</label>
		<?php echo form_textarea(array('name'=>'introtext_delivery', 'value' => $options['introtext_delivery'])); ?>
	</li>
	<li class="even">
		<label>Link</label>
		<?php echo form_input(array('name'=>'link_delivery', 'value' => $options['link_delivery'])); ?>
	</li>
</ul>
<br />
<ul>
	<label><b>Our Menu</b></label>
	<li class="even">
		<label>Title</label>
		<?php echo form_input(array('name'=>'title_menu', 'value' => $options['title_menu'])); ?>
	</li>
	<li class="even">
		<label>Introtext</label>
		<?php echo form_textarea(array('name'=>'introtext_menu', 'value' => $options['introtext_menu'])); ?>
	</li>
	<li class="even">
		<label>Link</label>
		<?php echo form_input(array('name'=>'link_menu', 'value' => $options['link_menu'])); ?>
	</li>
</ul>
<br />
<ul>
	<label><b>Visit Restaurant</b></label>
	<li class="even">
		<label>Title</label>
		<?php echo form_input(array('name'=>'title_retaurant', 'value' => $options['title_retaurant'])); ?>
	</li>
	<li class="even">
		<label>Introtext</label>
		<?php echo form_textarea(array('name'=>'introtext_retaurant', 'value' => $options['introtext_retaurant'])); ?>
	</li>
	<li class="even">
		<label>Link</label>
		<?php echo form_input(array('name'=>'link_retaurant', 'value' => $options['link_retaurant'])); ?>
	</li>
</ul>