<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Show RSS feeds in your site
 * 
 * @author  	Max Solution
 * @author		Max Solution Web Team
 * @package		PyroCMS\Core\Widgets
 */
class Widget_Menu_bottom extends Widgets
{

	public $title = 'Bottom Content';

	public $description = array(
		'en' => 'Bottom Widget no Images',
		'id' => 'Bottom widget tanpa gambar',
	);
	public $author = 'Max Solution';
	public $website = '';
	public $version = '1.0.0';

	public $fields = array(
		
		//catering
		array(
			
			'field' => 'title_catering',
			'label' => 'Title Catering',
			'rules' => 'required'
		),
		array(
			
			'field' => 'introtext_catering',
			'label' => 'Introtext Catering',
			'rules' => 'required'
		),
		array(
			
			'field' => 'link_catering',
			'label' => 'Link Catering',
			'rules' => 'required'
		),
		
		//delivery service
		array(
			
			'field' => 'title_delivery',
			'label' => 'Title Delivery',
			'rules' => 'required'
		),
		array(
			
			'field' => 'introtext_delivery',
			'label' => 'Introtext Delivery',
			'rules' => 'required'
		),
		array(
			
			'field' => 'link_delivery',
			'label' => 'Link Delivery',
			'rules' => 'required'
		),
		
		//our menu
		array(
			
			'field' => 'title_menu',
			'label' => 'Title Menu',
			'rules' => 'required'
		),
		array(
			
			'field' => 'introtext_menu',
			'label' => 'Introtext Menu',
			'rules' => 'required'
		),
		array(
			
			'field' => 'link_menu',
			'label' => 'Link Menu',
			'rules' => 'required'
		),
		
		//visit restaurant
		array(
			
			'field' => 'title_retaurant',
			'label' => 'Title Retaurant',
			'rules' => 'required'
		),
		array(
			
			'field' => 'introtext_retaurant',
			'label' => 'Introtext Retaurant',
			'rules' => 'required'
		),
		array(
			
			'field' => 'link_retaurant',
			'label' => 'Link Retaurant',
			'rules' => 'required'
		),
	);

	public function run($options) {
		
		/*if (empty($options['title']) && empty($options['introtext']) && empty($options['link'])) {
			
			return array(
				
				'title_catering' 		=> '',
				'introtext_catering'	=> '',
				'link_catering'			=> '',
				'title_delivery' 		=> '',
				'introtext_delivery'	=> '',
				'link_delivery'			=> '',
				'title_retaurant' 		=> '',
				'introtext_retaurant'	=> '',
				'link_retaurant'		=> '',
				'title_menu'	 		=> '',
				'introtext_menu'		=> '',
				'link_menu'				=> ''
			);
		}*/

		return array(
						
			'title_catering' 		=> $this->parser->parse_string($options['title_catering'], null, true),
			'introtext_catering'	=> $this->parser->parse_string($options['introtext_catering'], null, true),
			'link_catering'			=> $this->parser->parse_string($options['link_catering'], null, true),
			'title_delivery' 		=> $this->parser->parse_string($options['title_delivery'], null, true),
			'introtext_delivery'	=> $this->parser->parse_string($options['introtext_delivery'], null, true),
			'link_delivery'			=> $this->parser->parse_string($options['link_delivery'], null, true),
			'title_retaurant' 		=> $this->parser->parse_string($options['title_retaurant'], null, true),
			'introtext_retaurant'	=> $this->parser->parse_string($options['introtext_retaurant'], null, true),
			'link_retaurant'		=> $this->parser->parse_string($options['link_retaurant'], null, true),
			'title_menu'	 		=> $this->parser->parse_string($options['title_menu'], null, true),
			'introtext_menu'		=> $this->parser->parse_string($options['introtext_menu'], null, true),
			'link_menu'				=> $this->parser->parse_string($options['link_menu'], null, true)
		);
	}

}