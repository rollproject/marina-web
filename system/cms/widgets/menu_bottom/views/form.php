<ul>
	<label><b>Catering Widget</b></label>
	<li class="even">
		<label>Title</label>
		<?php echo form_input(array('name'=>'title_catering', 'value' => $options['title_catering'], 'style' => 'width: 400px;')); ?>
	</li>
	<li class="even">
		<label>Slogan</label>
		<?php echo form_input(array('name'=>'introtext_catering', 'value' => $options['introtext_catering'], 'style' => 'width: 400px;')); ?>
	</li>
	<li class="even">
		<label>Link</label>
		<?php echo form_input(array('name'=>'link_catering', 'value' => $options['link_catering'], 'style' => 'width: 400px;')); ?>
	</li>
</ul>
<br />
<ul>
	<label><b>Delivery Service</b></label>
	<li class="even">
		<label>Title</label>
		<?php echo form_input(array('name'=>'title_delivery', 'value' => $options['title_delivery'], 'style' => 'width: 400px;')); ?>
	</li>
	<li class="even">
		<label>Slogan</label>
		<?php echo form_input(array('name'=>'introtext_delivery', 'value' => $options['introtext_delivery'], 'style' => 'width: 400px;')); ?>
	</li>
	<li class="even">
		<label>Link</label>
		<?php echo form_input(array('name'=>'link_delivery', 'value' => $options['link_delivery'], 'style' => 'width: 400px;')); ?>
	</li>
</ul>
<br />
<ul>
	<label><b>Our Menu</b></label>
	<li class="even">
		<label>Title</label>
		<?php echo form_input(array('name'=>'title_menu', 'value' => $options['title_menu'], 'style' => 'width: 400px;')); ?>
	</li>
	<li class="even">
		<label>Slogan</label>
		<?php echo form_input(array('name'=>'introtext_menu', 'value' => $options['introtext_menu'], 'style' => 'width: 400px;')); ?>
	</li>
	<li class="even">
		<label>Link</label>
		<?php echo form_input(array('name'=>'link_menu', 'value' => $options['link_menu'], 'style' => 'width: 400px;')); ?>
	</li>
</ul>
<br />
<ul>
	<label><b>Visit Restaurant</b></label>
	<li class="even">
		<label>Title</label>
		<?php echo form_input(array('name'=>'title_retaurant', 'value' => $options['title_retaurant'], 'style' => 'width: 400px;')); ?>
	</li>
	<li class="even">
		<label>Slogan</label>
		<?php echo form_input(array('name'=>'introtext_retaurant', 'value' => $options['introtext_retaurant'], 'style' => 'width: 400px;')); ?>
	</li>
	<li class="even">
		<label>Link</label>
		<?php echo form_input(array('name'=>'link_retaurant', 'value' => $options['link_retaurant'], 'style' => 'width: 400px;')); ?>
	</li>
</ul>