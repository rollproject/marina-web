<ul>
	<li class="source">
		<a href="<?php echo site_url($link_retaurant); ?>">
		<div class="top-info">
			<i class="ico"></i>
			<span class="title"><?php echo $title_retaurant; ?></span>
			<span class="slogan"><?php echo $introtext_retaurant; ?></span>
		</div>
		<!--
		<div class="thumbs">
			<img src="{{ theme:image_url file="box-4-1.jpg" }}">
		</div>
		-->
		</a>
	</li>
	<li class="proffesionaly">
		<a href="<?php echo site_url($link_catering); ?>">
		<div class="top-info">
			<i class="ico"></i>
			<span class="title"><?php echo $title_catering; ?></span>
			<span class="slogan"><?php echo $introtext_catering; ?></span>
		</div>
		<!--
		<div class="thumbs">
			<img src="{{ theme:image_url file="box-4-2.jpg" }}">
		</div>
		-->
		</a>
	</li>
	<li class="ready">
		<a href="<?php echo site_url($link_delivery); ?>">
		<div class="top-info">
			<i class="ico"></i>
			<span class="title"><?php echo $title_delivery; ?></span>
			<span class="slogan"><?php echo $introtext_delivery; ?></span>
		</div>
		<!--
		<div class="thumbs">
			<img src="{{ theme:image_url file="box-4-3.jpg" }}">
		</div>
		-->
		</a>
	</li>
	<li class="our">
		<a href="<?php echo site_url($link_menu); ?>">
		<div class="top-info">
			<i class="ico"></i>
			<span class="title"><?php echo $title_menu; ?></span>
			<span class="slogan"><?php echo $introtext_menu; ?></span>
		</div>
		<!--
		<div class="thumbs">
			<img src="{{ theme:image_url file="box-4-4.jpg" }}">
		</div>
		-->
		</a>
	</li>
	<div class="clear"></div>
</ul>