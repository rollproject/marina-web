<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
class Module_Product extends Module {

	public $version = '1.0.0';

	public function info()
	{
		return array(
			'name' => array(
			
				'en' => 'Marina Resto Eccomerce',
			),
			
			'description' => array(
			
				'en' => 'Module Marina Resto Food Eccomerce.',
			),
			'frontend'	=> TRUE,
			'backend'	=> TRUE,
			'skip_xss'	=> TRUE,
			'menu'		=> 'content',
			'author'	=> 'Max Solution',
		
			'roles'		=> array(
			
				'admin_product'
			),
			
			'sections' => array(
			    'product' => array(
				    'name'		=> 'Dasboard',
				    'uri'		=> 'admin/product',
				    'shortcuts'	=> array(
						array(
					 	   'name'	=> 'View Dasboard',
						   'uri'	=> 'admin/product',
						   'class'	=> 'add'
						),					
					)
				),
			    'category' => array(
				    'name'		=> 'Category',
				    'uri'		=> 'admin/product/category/get_all',
				    'shortcuts'	=> array(
						array(
					 	   'name'	=> 'Create Category',
						   'uri'	=> 'admin/product/category',
						   'class'	=> 'add'
						),
					)
				),
				'item' => array(
				    'name'		=> 'Product Item',
				    'uri'		=> 'admin/product/item/',
				    'shortcuts'	=> array(
						array(
					 	   'name'	=> '"Add Product Item"',
						   'uri'	=> 'admin/product/item/add',
						   'class'	=> 'add'
						),
					)
				),
				'choice' => array(
				    'name'		=> 'Master Choice',
				    'uri'		=> 'admin/product/choice/get_all',
				    'shortcuts'	=> array(
						array(
					 	   'name'	=> 'Add Choice Item',
						   'uri'	=> 'admin/product/choice/add',
						   'class'	=> 'add'
						),
					)
				),
				'catering' => array(
				    'name'		=> 'Master Catering',
				    'uri'		=> 'admin/product/catering/get_all',
				    'shortcuts'	=> array(
						array(
					 	   'name'	=> 'Add Catering',
						   'uri'	=> 'admin/product/catering/add',
						   'class'	=> 'add'
						),
					)
				),
			)
		);
	}

	public function install() {

		/*
		$this->dbforge->drop_table('product_category');
        $this->dbforge->drop_table('product_item');
		$this->dbforge->drop_table('product_choice');
		$this->dbforge->drop_table('product_list_choice');
		$this->dbforge->drop_table('product_master_choice');
		$this->dbforge->drop_table('product_order');
		$this->dbforge->drop_table('product_list_order');
		$this->dbforge->drop_table('product_temp_order');
		*/
		
		$this->dbforge->drop_table('product_temp_order');
		$this->dbforge->drop_table('product_order');
		$this->dbforge->drop_table('product_list_order');
		
		$product_category_fields = array(
			
			'id_category' 	=> array('type' => 'INT', 'auto_increment' => true, 'primary' => true),
			'name_category' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false),
			'slug' 			=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => true),
			'description' 	=> array('type' => 'TEXT', 'null' => true),
			'id_parent' 	=> array('type' => 'INT', 'null' => true),
			'status'		=> array('type' => 'INT', 'constraint' => 1, 'null' => true),
			'pic'			=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => true),
		);
		
		$product_item_fields = array(
			
			'id_item' 		=> array('type' => 'INT', 'auto_increment' => true, 'primary' => true),
			'id_category' 	=> array('type' => 'INT', 'null' => false),
			'name_item' 	=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false),
			'slug' 			=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => true),
			'price_item' 	=> array('type' => 'INT', 'null' => false),
			'status'		=> array('type' => 'INT', 'constraint' => 1, 'null' => true),
			'pic'			=> array('type' => 'VARCHAR', 'constraint' => 200, 'null' => true),
			'description'	=> array('type' => 'TEXT', 'null' => true),
		);
		
		$product_choice_fields = array(
			
			'id_choice' 	=> array('type' => 'INT', 'auto_increment' => true, 'primary' => true),
			'name_choice' 	=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => true),
		);
		
		$product_list_choice_fields = array(
			
			'id_list' 			=> array('type' => 'INT', 'auto_increment' => true, 'primary' => true),
			'id_choice' 		=> array('type' => 'INT', 'null' => false),
			'name_list_choice' 	=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false),
		);
		
		$product_order_fields = array(
			
			'id_order' 			=> array('type' => 'INT', 'auto_increment' => true, 'primary' => true),
			'date_order' 		=> array('type' => 'DATETIME'),
			'name_customer' 	=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => true),
			'email_customer' 	=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => true),
			'address_customer'	=> array('type' => 'TEXT', 'null' => true),
			'phone_customer'	=> array('type' => 'VARCHAR', 'constraint' => 20, 'null' => true),
			'status_order'		=> array('type' => 'VARCHAR', 'constraint' => 20, 'null' => true),
			'payment_method'	=> array('type' => 'VARCHAR', 'constraint' => 50, 'null' => true),
			'service_method'	=> array('type' => 'VARCHAR', 'constraint' => 50, 'null' => true),
			'total'				=> array('type' => 'INT', 'null' => true),
			'note'				=> array('type' => 'TEXT', 'null' => true)
		);
		
		$product_list_order_fields = array(
			
			'id_list_order' 	=> array('type' => 'INT', 'auto_increment' => true, 'primary' => true),
			'id_order' 			=> array('type' => 'INT', 'null' => false),
			'id_item' 			=> array('type' => 'INT', 'null' => false),
			'qty'				=> array('type' => 'INT', 'null' => false),
			'price_per_item'	=> array('type' => 'INT', 'null' => false),
			'choice_method' 	=> array('type' => 'TEXT', 'null' => true),
		);
		
		$product_master_choice_fields = array(
			
			'id_master' 		=> array('type' => 'INT', 'auto_increment' => true, 'primary' => true),
			'id_item'			=> array('type' => 'INT', 'null' => false),
			'id_choice'			=> array('type' => 'INT', 'null' => false),
		);
		
		$product_temp_order_fields = array(
			
			'id_temp_order' => array('type' => 'INT', 'auto_increment' => true, 'primary' => true),
			'id_session'	=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false),
			'id_item' 		=> array('type' => 'INT', 'null' => false),
			'price_per_item'=> array('type' => 'INT', 'null' => false),
			'qty' 			=> array('type' => 'INT', 'null' => false),
			'choice'		=> array('type' => 'TEXT', 'null' => true),
		);
		
		$product_catering_fields = array(
			
			'id_catering' 	=> array('type' => 'INT', 'auto_increment' => true, 'primary' => true),
			'name_catering'	=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false),
			'price'			=> array('type' => 'INT', 'null' => false),
			'desc' 			=> array('type' => 'TEXT', 'null' => false),
		);
		
		$product_temp_catering_fields = array(
			
			'id_temp_order' => array('type' => 'INT', 'auto_increment' => true, 'primary' => true),
			'id_session'	=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false),
			'id_catering'	=> array('type' => 'INT', 'null' => false),
			'price'			=> array('type' => 'INT', 'null' => false),
			'qty' 			=> array('type' => 'INT', 'null' => false)
		);
		
		return $this->install_tables(array(
			
			'product_category'		=> $product_category_fields, 
			'product_item'			=> $product_item_fields,
			'product_choice'		=> $product_choice_fields,
			'product_list_choice'	=> $product_list_choice_fields,
			'product_order'			=> $product_order_fields,
			'product_list_order'	=> $product_list_order_fields,
			'product_master_choice'	=> $product_master_choice_fields,
			'product_temp_order'	=> $product_temp_order_fields,
			'product_catering'		=> $product_catering_fields,
			'product_temp_catering' => $product_temp_catering_fields,
		));		
	}

	public function uninstall() {
		
		return TRUE;
	}


	public function upgrade($old_version) {
		
		TRUE;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
}
/* End of file details.php */