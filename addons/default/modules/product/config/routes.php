<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a store module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-store
 * @subpackage 	Product
**/

// admin
$route['product/admin/category(/:any)?']	= 'admin_category$1';
$route['product/admin/item(/:any)?']		= 'admin_item$1';
$route['product/admin/choice(/:any)?']		= 'admin_choice$1';
$route['product/admin/catering(/:any)?']	= 'admin_catering$1';

//front end
//$route['product/category']					= 'front_end$1';
$route['product/front_end(/:any)?']			= 'front_end$1';
