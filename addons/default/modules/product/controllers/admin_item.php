<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
class Admin_item extends Admin_Controller {
	
	protected $section = 'item';
	
	public function __construct() {
		
		parent::__construct();

		$this->load->model('product_m');
		$this->load->library('form_validation');
		$this->load->helper(array('date', 'url'));
		
	}

	public function index() {
		
		$this->get_all();
	}

	function get_all() {
		
		$title		= 'Product Item';
		$url		= site_url().'/product/admin_item/add';
		$total_rows = $this->product_m->count_all_('product_item');
		$pagination = create_pagination('product/admin_item/get_all', $total_rows);	
		
		$item 		= $this->product_m
					  ->limit($pagination['limit'], $pagination['offset'])
					  ->get_all_item();
				
		$this->template
			 ->title($title)
			 ->set('item', $item)
			 ->set('url', $url)
			 ->set('judul', $title)
			 ->set('pagination', $pagination)
			 ->build('admin/item/item');
	}
	
	function delete($id_item) {
	
		$this->product_m->delete_('product_item', 'id_item', $id_item);
		
		redirect(site_url().'/product/admin_item');
	}
	
	function add() {
		
		$title			= "Add Product Item";
		$form_action	= site_url().'/product/admin_item/add_process'; 
		$category		= $this->product_m->get_('name_category', 'product_category');
		$status			= '';
		$pic 			= '';
		$id_parent		= '';
		
		foreach($category as $row) {
			$data['0'] = 'Select Category';
			$data[$row->id_category] = $row->name_category;
			
		}

		$this->template
			 ->title($title)
			 ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			 ->set('title', $title)
			 ->set('url', $form_action)
			 ->set('category', $category)
			 ->set('options_category', $data)
			 ->set('status', $status)
			 ->set('pic', $pic)
			 ->set('id_parent', $id_parent)
			 ->build('admin/item/form');
		
	}
	
	function add_process() {
	
		$title			= "Add Product Item";
		$form_action	= site_url().'/product/admin_item/add_process'; 
		$category		= $this->product_m->get_('name_category', 'product_category');
		$status			= '';
		$pic 			= '';
		$id_parent		= '';
		
		$nama_asli 					= $_FILES['userfile']['name'];	
		$judul 						= $this->input->post('name_item',TRUE);
		$config['file_name'] 		= $judul.'_'.'_'.$nama_asli;
		$config['upload_path'] 		= getcwd().'/uploads/product/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size']			= '1024';
		$config['max_width']  		= '1600';
		$config['max_height']  		= '1200';
		
		$this->load->library('upload', $config);
		
		$this->form_validation->set_rules('id_category', 'Category', '');
		$this->form_validation->set_rules('name_item', 'Name Product item', 'required');
		$this->form_validation->set_rules('slug', 'Slug', '');
		$this->form_validation->set_rules('price_item', 'Price', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');
		$this->form_validation->set_rules('pic', 'Pictures', '');
		$this->form_validation->set_rules('description', 'Description', '');
		
		if ($this->form_validation->run() == TRUE) {
			
			if($this->upload->do_upload()){
					
				$get_name = $this->upload->data();
				$nama_foto = $get_name['file_name'];
				
				$item = array(
			
					'id_category' 	=> $this->input->post('id_category'),
					'name_item'		=> $this->input->post('name_item'),
					'slug'			=> $this->input->post('slug'),
					'price_item'	=> $this->input->post('price_item'),
					'status'		=> $this->input->post('status'),
					'pic'			=> $nama_foto,
					'description'	=> $this->input->post('description')
				);	
					
				$this->createThumbnail($nama_foto);
				$this->product_m->insert_('product_item', $item);			
				redirect(site_url().'/product/admin_item');
					
			}else{
							
						
				$item = array(
			
					'id_category' 	=> $this->input->post('id_category'),
					'name_item'		=> $this->input->post('name_item'),
					'slug'			=> $this->input->post('slug'),
					'price_item'	=> $this->input->post('price_item'),
					'status'		=> $this->input->post('status'),
					'pic'			=> '',
					'description'	=> $this->input->post('description')
				);	
					
				$this->product_m->insert_('product_item', $item);			
				redirect(site_url().'/product/admin_item');
			}
					
		}else {
				
			$this->template
			 	 ->title('Add Category Product')
				 ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			  	 ->set('title', $title)
				 ->set('category', $category)
				 ->set('options_category', $data['options_category'])
			 	 ->set('url', $form_action)
				 ->set('status', $status)
			 	 ->set('pic', $pic)
				 ->set('id_parent', $id_parent)
			 	 ->build('admin/item/form');
		}		
	}
	
	public function upload_img() {
		
       $type 	= $_FILES['thumbnail_file']['type'];
       $type 	= explode('/',$type);
       $type 	= $type[1];
       $tmp 	= $_FILES['thumbnail_file']['tmp_name'];
       $section = $this->uri->segment(3);
       $dir 	= 'uploads/product/item/';
       $upload 	= $this->session->userdata('thumb').'.'.$type;
       $this->session->set_userdata('upload_thumbnail',$upload);
      
       if(file_exists($upload)){
            	
            unlink($upload);      
       }
       
       if(move_uploaded_file($tmp, $dir.$upload)){
            	
            list($width, $height) = getimagesize($dir.$upload);
            chmod($dir.$upload, 0775);
            echo $upload.'?'.rand(0,999999999999);
       
		}else{
            	
            echo 'false';
       }
    }

	function update($id_item) {
	
		$title			= "Update Product Item";
		$form_action	= site_url().'/product/admin_item/update_process';
		
		$category		= $this->product_m->get_('name_category', 'product_category');
		
		foreach($category as $row) {
			$data['0'] = 'pilih kategori';
			$data[$row->id_category] = $row->name_category;
			
		}
											
		$item 			= $this->product_m->get_by_id_('product_item', 'id_item', $id_item);
		$this->session->set_userdata('id_item', $item->id_item);
		
		$name_item		= $item->name_item;
		$price_item		= $item->price_item;
		$slug			= $item->slug;
		$description	= $item->description;
		$status			= $item->status;
		$pic			= $item->pic;
		$id_category	= $item->id_category;
		$pic			= $item->pic;
		$status			= $item->status;
		
		$this->template
			 	 ->title($title)
				 ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			  	 ->set('title', $title)
			 	 ->set('url', $form_action)
				 ->set('name_item', $name_item)
				 ->set('slug', $slug)
				 ->set('price_item', $price_item)
				 ->set('description', $description)
				 ->set('status', $status)
				 ->set('pic', $pic)
				 ->set('id_category', $id_category)
				 ->set('pic', $pic)
				 ->set('status', $status)
				 ->set('options_category', $data)
			 	 ->build('admin/item/form');
	}
	
	function update_process() {
	
		$title			= "Update Product Item";
		$form_action	= site_url().'/product/admin_item/update_process';
											
		$category		= $this->product_m->get_('name_category', 'product_category');
		
		//$arr_nama_cat[0] = 'pilih kategori';
		//$arr_id_cat[0] = '0';
		foreach($category as $row) {
			$data['options_category']['0'] = 'pilih kategori';
			$data['options_category'][$row->id_category] = $row->name_category;
			
			
			//$arr_nama_cat[] = $row->name_category;
			//$arr_id_cat[] = $row->id_category;
			
		}
		
		$item 			= $this->product_m->get_by_id_('product_item', 'id_item', $this->session->userdata('id_item'));
		$this->session->set_userdata('id_item', $item->id_item);
		
		$name_item		= $item->name_item;
		$price_item		= $item->price_item;
		$slug			= $item->slug;
		$description	= $item->description;
		$status			= $item->status;
		$pic			= $item->pic;
		$id_category	= $item->id_category;
		$pic			= $item->pic;
		$status			= $item->status;
		
		$nama_asli 					= $_FILES['userfile']['name'];	
		$judul 						= $this->input->post('kategori',TRUE);
		$config['file_name'] 		= $judul.'_'.'_'.$nama_asli;
		$config['upload_path'] 		= getcwd().'/uploads/product/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size']			= '1024';
		$config['max_width']  		= '1600';
		$config['max_height']  		= '1200';
		
		$this->load->library('upload', $config);
										
		$this->form_validation->set_rules('id_category', 'Category', 'required');
		$this->form_validation->set_rules('name_item', 'Name Product item', 'required');
		$this->form_validation->set_rules('slug', 'Slug', '');
		$this->form_validation->set_rules('price_item', 'Price', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');
		$this->form_validation->set_rules('pic', 'Pictures', '');
		$this->form_validation->set_rules('description', 'Description', '');
		
		if ($this->form_validation->run() == TRUE) {
				
			if($this->upload->do_upload()){
					
				$get_name = $this->upload->data();
				$nama_foto = $get_name['file_name'];
				
				$item = array(
			
					'id_category' 	=> $this->input->post('id_category'),
					'name_item'		=> $this->input->post('name_item'),
					'slug'			=> $this->input->post('slug'),
					'price_item'	=> $this->input->post('price_item'),
					'status'		=> $this->input->post('status'),
					'pic'			=> $nama_foto,
					'description'	=> $this->input->post('description')
				);
				
				$this->createThumbnail($nama_foto);
				$this->product_m->update_('id_item', $this->session->userdata('id_item'), 'product_item', $item);
				redirect(site_url().'/product/admin_item');
				
			}else{
						
				$item = array(
			
					'id_category' 	=> $this->input->post('id_category'),
					'name_item'		=> $this->input->post('name_item'),
					'slug'			=> $this->input->post('slug'),
					'price_item'	=> $this->input->post('price_item'),
					'status'		=> $this->input->post('status'),
					'pic'			=> $this->input->post('pic'),
					'description'	=> $this->input->post('description')
				);		
				
				$this->product_m->update_('id_item', $this->session->userdata('id_item'), 'product_item', $item);
				redirect(site_url().'/product/admin_item');
			}
		
		}else {
			
			$this->template
				 ->title($title)
				 ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
				 ->set('title', $title)
				 ->set('url', $form_action)
				 ->set('name_item', $name_item)
				 ->set('slug', $slug)
				 ->set('price_item', $price_item)
				 ->set('description', $description)
				 ->set('status', $status)
				 ->set('pic', $pic)
				 ->set('id_category', $id_category)
				 ->set('options_category_', $data['options_category'])
				 ->set('pic', $pic)
				 ->set('status', $status)
				 ->build('admin/item/form');
		}
	}

	function createThumbnail($fileName) {
			
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['image_library'] 	= 'gd2';
		$config['source_image'] 	= getcwd().'/uploads/product/' . $fileName;
		$config['new_image'] 		= getcwd().'/uploads/product/thumb/'. $fileName;
		$config['maintain_ratio'] 	= TRUE;
		$config['width'] 			= 150;
		$config['height'] 			= 150;
		
		$this->load->library('image_lib');
		$this->image_lib->clear();
		$this->image_lib->initialize($config);
			
		if(!$this->image_lib->resize()) { 
			echo $this->image_lib->display_errors();
			//var_dump($config);die();
		}
	}

	function format_idr($price) {
			
		$rupiah = number_format($price,2,',','.');
		return $rupiah;

	}
}