<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
class Admin_catering extends Admin_Controller {
	
	protected $validation_rules = array(
		
		'name' => array(
			'field' => 'name_catering',
			'label' => 'Name',
			'rules' => 'trim|htmlspecialchars|required'
		),
		array(
			'field' => 'desc',
			'label' => 'Catering Description',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'price',
			'label' => 'Price',
			'rules' => 'trim|required'
		),		
	);
	
	
	public function __construct() {
		
		parent::__construct();

		$this->load->model('product_m');
		$this->load->library('form_validation');
		$this->load->helper(array('html','url')); 
		$this->form_validation->set_rules($this->validation_rules);
		
	}

	public function index() {
		
		$this->get_all();
	}

	function get_all() {
		
		$title		= 'Catering';
		$url		= site_url().'/product/admin_catering/add';
		
		$total_rows = $this->product_m->count_all_('product_catering');
		$pagination = create_pagination('admin/admin_catering/get_all', $total_rows);
		$item 		= $this->product_m
					  ->limit($pagination['limit'], $pagination['offset'])->get_all_('product_catering', 'name_catering');
				
		$this->template
			 ->title($title)
			 ->set('item', $item)
			 ->set('url', $url)
			 ->set('judul', $title)
			 ->set('pagination', $pagination)
			 ->build('admin/catering/catering');
	}
	
	function delete($id_catering) {
	
		$this->product_m->delete_('product_catering', 'id_catering', $id_catering);
		
		redirect(site_url().'/product/admin_catering');
	}
	
	function add() {
		
		$title			= "Add Product Item";
		$form_name		= "catering_form";
		$form_action	= site_url().'/product/admin_catering/add_process'; 
		
		$this->template
			 ->title($title)
			 ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			 ->set('title', $title)
			 ->set('url', $form_action)
			 ->set('form_name', $form_name)
			 ->build('admin/catering/form');
		
	}
	
	function add_process() {
	
		$title			= "Add Product Item";
		$form_name		= "catering_form";
		$form_action	= site_url().'/product/admin_catering/add_process'; 
		
		if ($this->form_validation->run() == TRUE) {
			
			$catering = array(
			
				'name_catering'	=> $this->input->post('name_catering'),
				'price'			=> $this->input->post('price'),
				'desc'			=> $this->input->post('desc')
			);	
			
			$this->product_m->insert_('product_catering', $catering);			
			redirect(site_url().'/product/admin_catering');
		
		}else {
				
			$this->template
			 	 ->title($title)
				 ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
				 ->set('title', $title)
				 ->set('url', $form_action)
				 ->set('form_name', $form_name)
				 ->build('admin/catering/form');
		}		
	}

	function update($id_catering) {
	
		$title			= "Update catering";
		$form_name		= "catering_form";
		$form_action	= site_url().'/product/admin_catering/update_process';
											
		$item 			= $this->product_m->get_by_id('product_catering', 'id_catering', $id_catering);
		$this->session->set_userdata('id_catering', $item->id_catering);
		
		$name_catering	= $item->name_catering;
		$price			= $item->price;
		$desc			= $item->desc;
		
		$this->template
			 	 ->title($title)
			  	 ->set('title', $title)
				 ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			 	 ->set('url', $form_action)
 				 ->set('form_name', $form_name)
				 ->set('name_catering', $name_catering)
				 ->set('price', $price)
				 ->set('desc', $desc)
			 	 ->build('admin/catering/form');
	}
	
	function update_process() {
	
		$title			= "Update catering";
		$form_name		= "catering_form";
		$form_action	= site_url().'/product/admin_catering/update_process';
											
		$item 			= $this->product_m->get_by_id('product_catering', 'id_catering', $this->session->userdata('id_catering'));
		$this->session->set_userdata('id_catering', $item->id_catering);
		
		$name_catering	= $item->name_catering;
		$price			= $item->price;
		$desc			= $item->desc;
										
		if ($this->form_validation->run() == TRUE) {
			
			$catering = array(
			
				'name_catering'	=> $this->input->post('name_catering'),
				'price'			=> $this->input->post('price'),
				'desc'			=> $this->input->post('desc')
			);	
			
			$this->product_m->update_('id_catering', $this->session->userdata('id_catering'), 'product_catering', $catering);
			redirect(site_url().'/product/admin_catering');
		
		}else {
			
			$this->template
			 	 ->title($title)
			  	 ->set('title', $title)
				 ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			 	 ->set('url', $form_action)
 				 ->set('form_name', $form_name)
				 ->set('name_catering', $name_catering)
				 ->set('price', $price)
				 ->set('desc', $desc)
			 	 ->build('admin/catering/form');
		}
	}
}