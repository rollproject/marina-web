<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
class Admin_category extends Admin_Controller
{
	protected $section = 'category';

	public function __construct()
	{
		parent::__construct();

		$this->load->model('product_m');
		$this->load->library('form_validation', 'Excel_reader');
		$this->load->helper('date', 'url');
	}

	public function index() {
		
		$this->get_all();
	}

	function get_all() {
		
		$title		= 'Product Category';
		$url		= site_url().'/product/admin_category/add';
		$total_rows = $this->product_m->count_all_('product_category');
		$pagination = create_pagination('product/admin_category/get_all', $total_rows);	
		
		$category 	= $this->product_m
					->limit($pagination['limit'], $pagination['offset'])
					->get_all_($table = 'product_category', $field = 'id_parent');
		
		
		$this->template
			 ->title($this->module_details['name'])
			 ->set('category', $category)
			 ->set('url', $url)
			 ->set('pagination', $pagination)
			 ->build('admin/category/category');
	}
	
	function delete($id_category) {
	
		$this->product_m->delete_('product_category', 'id_category', $id_category);
		
		redirect(site_url().'/product/admin_category');
	}
	
	function add() {
		
		$title			= "Add Category Product";
		$form_action	= site_url().'/product/admin_category/add_process'; 
		$category		= $this->product_m->get_('name_category', 'product_category');
		$status			= '';
		$pic 			= '';
		$id_parent		= '';
		
		foreach($category as $row) {
			
			$data['0'] = 'pilih kategori';
			$data[$row->id_category] = $row->name_category;
			
		}
		
		
		$this->template
			 ->title('Add Category Product')
			 ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			 ->set('title', $title)
			 ->set('url', $form_action)
			 ->set('category', $category)
			 ->set('options_category', $data)
			 ->set('status', $status)
			 ->set('id_parent', $id_parent)
			 ->set('pic', $pic)
			 ->build('admin/category/form');
		
	}
	
	function add_process() {
	
		$title			= "Add Category Product";
		$form_action	= site_url().'/product/admin_category/add_process'; 
		$cate 			= $this->product_m->get_category();
		$status			= '';
		$pic 			= '';
		$id_parent		= '';
		
		$nama_asli 					= $_FILES['userfile']['name'];	
		$judul 						= $this->input->post('kategori',TRUE);
		$config['file_name'] 		= $judul.'_'.'_'.$nama_asli;
		$config['upload_path'] 		= getcwd().'/uploads/category/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size']			= '1024';
		$config['max_width']  		= '1600';
		$config['max_height']  		= '1200';
		
		$this->load->library('upload', $config);
		
		$this->form_validation->set_rules('kategori', 'Kategori', 'required');
		$this->form_validation->set_rules('slug', 'Slug', '');
		$this->form_validation->set_rules('description', 'Description', '');
		
		if ($this->form_validation->run() == TRUE) {
			
			if($this->upload->do_upload()){
					
				$get_name = $this->upload->data();
				$nama_foto = $get_name['file_name'];
				
				$category = array(
					
					'name_category' => $this->input->post('kategori'),
					'description'	=> $this->input->post('description'),
					'status'		=> $this->input->post('status'),
					'pic'			=> $nama_foto,
					'id_parent'		=> $this->input->post('id_parent')
				
				);
				
				$this->createThumbnail($nama_foto);
				$this->product_m->insert_('product_category', $category);
				//var_dump($this->createThumbnail($nama_foto)); die();			
				redirect(site_url().'/product/admin_category');
					
			}else{
							
						
				$category = array(
					
					'name_category' => $this->input->post('kategori'),
					'description'	=> $this->input->post('description'),
					'status'		=> $this->input->post('status'),
					'pic'			=> $this->input->post('pic'),
					'id_parent'		=> $this->input->post('id_parent')
				
				);
					
				$this->product_m->insert_('product_category', $category);			
				redirect(site_url().'/product/admin_category');
			}
		
		}else {
				
			$this->template
			 	 ->title('Add Category Product')
				 ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			  	 ->set('title', $title)
				 ->set('cate', $cate)
			 	 ->set('url', $form_action)
				 ->set('status', $status)
				 ->set('pic', $pic)
				 ->set('id_parent', $id_parent)
			 	 ->build('admin/category/form');
		}		
	}
	
	function update($id_category) {
	
		$title			= "Update Category Product";
		$form_action	= site_url().'/product/admin_category/update_process';
		
		$category		= $this->product_m->get_('name_category', 'product_category');
		
		foreach($category as $row) {
			$data['0'] = 'pilih kategori';
			$data[$row->id_category] = $row->name_category;
			
		}
									
		$cate = $this->product_m->get_by_id_('product_category', 'id_category', $id_category);
		$this->session->set_userdata('id_category', $cate->id_category);
		$name_category 	= $cate->name_category;
		$slug			= $cate->slug;
		$id_parent		= $cate->id_parent;
		$description	= $cate->description;
		$pic			= $cate->pic;
		$status			= $cate->status;
		$pic			= $cate->pic;
		$status			= $cate->status;
		
		$this->template
			 	 ->title($title)
				 ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			  	 ->set('title', $title)
			 	 ->set('url', $form_action)
				 ->set('name_category', $name_category)
				 ->set('slug', $slug)
				 ->set('id_parent', $id_parent)
				 ->set('description', $description)
				 ->set('category', $category)
				 ->set('id_parent', $id_parent)
				 ->set('pic', $pic)
				 ->set('status', $status)
				 ->set('options_category', $data)
				 ->build('admin/category/form');
	}
	
	function update_process() {
	
		$title			= "Update Category Product";
		$form_action	= site_url().'/product/admin_category/update_process';
		
		$cate = $this->product_m->get_by_id_('product_category', 'id_category', $this->session->userdata('id_category'));
		$name_category 	= $cate->name_category;
		$slug			= $cate->slug;
		$id_parent		= $cate->id_parent;
		$description	= $cate->description;
		$pic			= $cate->pic;
		$status			= $cate->status;
		$pic			= $cate->pic;
		$status			= $cate->status;
		
		$nama_asli 					= $_FILES['userfile']['name'];	
		$judul 						= $this->input->post('kategori',TRUE);
		$config['file_name'] 		= $judul.'_'.'_'.$nama_asli;
		$config['upload_path'] 		= getcwd().'/uploads/category/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size']			= '1024';
		$config['max_width']  		= '1600';
		$config['max_height']  		= '1200';
		
		$this->load->library('upload', $config);
										
		$this->form_validation->set_rules('kategori', 'Kategori', 'required');
		$this->form_validation->set_rules('slug', 'Slug', '');
		$this->form_validation->set_rules('description', 'Description', '');
		
		if ($this->form_validation->run() == TRUE) {
				
			if($this->upload->do_upload()){
					
				$get_name = $this->upload->data();
				$nama_foto = $get_name['file_name'];
				
				$category = array(
					
					'name_category' => $this->input->post('kategori'),
					'description'	=> $this->input->post('description'),
					'status'		=> $this->input->post('status'),
					'pic'			=> $nama_foto,
					'id_parent'		=> $this->input->post('id_parent')
				
				);
				
				$this->createThumbnail($nama_foto);
				$this->product_m->update_('id_category', $this->session->userdata('id_category'), 'product_category', $category);
				//$this->session->set_flashdata('message', 'data berhasil di ubah');
				redirect(site_url().'/product/admin_category');
				
			}else{
						
				$category = array(
					
					'name_category' => $this->input->post('kategori'),
					'description'	=> $this->input->post('description'),
					'status'		=> $this->input->post('status'),
					'pic'			=> $this->input->post('pic'),
					'id_parent'		=> $this->input->post('id_parent')
				
				);		
				
				$this->product_m->update_('id_category', $this->session->userdata('id_category'), 'product_category', $category);
				//$this->session->set_flashdata('message', 'data berhasil di ubah');
				redirect(site_url().'/product/admin_category');
			}
				
		}else {
				
			$this->template
			 	 ->title($title)
				 ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			  	 ->set('title', $title)
			 	 ->set('url', $form_action)
				 ->build('admin/category/form');
		}
	}

	function createThumbnail($fileName) {
			
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['image_library'] 	= 'gd2';
		$config['source_image'] 	= getcwd().'/uploads/category/' . $fileName;
		$config['new_image'] 		= getcwd().'/uploads/category/thumb/'. $fileName;
		$config['maintain_ratio'] 	= TRUE;
		$config['width'] 			= 150;
		$config['height'] 			= 150;
		
		$this->load->library('image_lib');
		$this->image_lib->clear();
		$this->image_lib->initialize($config);
			
		if(!$this->image_lib->resize()) { 
			echo $this->image_lib->display_errors();
			//var_dump($config);die();
		}
	}
	
	public function export_page(){
		
		$title			= "Update Category Product";
		$form_action	= site_url().'/product/admin_category/export_process';
		
		 $this->template
            ->append_js('module::jquery.ui.datepicker.js')
			->title('Marina Export Menu')
			->set('url', $form_action)
			->set('title', $title)
			->build('admin/category/export_form');
				
	}

	public function export_process() {
		
		$title			= "Update Category Product";
		$form_action	= site_url().'/product/admin_category/update_process';
		
		$this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
            
        $query = "SELECT * FROM default_product_category";
        $data = $this->db->query($query)->result();

        $count = 2;
        $this->excel->getActiveSheet()->setCellValue('A1', 'NO');
        $this->excel->getActiveSheet()->setCellValue('B1', 'ID Category');   
        $this->excel->getActiveSheet()->setCellValue('C1', 'Name Category');
        $this->excel->getActiveSheet()->setCellValue('D1', 'Status');
        $this->excel->getActiveSheet()->setCellValue('E1', 'Description');
		$this->excel->getActiveSheet()->setCellValue('F1', 'ID Parent');
		
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
            
        $no=1;
        foreach($data as $dt){
        	$this->excel->getActiveSheet()->setCellValue('A'.$count, $no);   
            $this->excel->getActiveSheet()->setCellValue('B'.$count, $dt->id_category); 
            $this->excel->getActiveSheet()->setCellValue('C'.$count, $dt->name_category);
			$this->excel->getActiveSheet()->setCellValue('D'.$count, $dt->status);
			$this->excel->getActiveSheet()->setCellValue('E'.$count, $dt->description);
            $this->excel->getActiveSheet()->setCellValueExplicit('E'.$count, $dt->description, PHPExcel_Cell_DataType::TYPE_STRING);
            $this->excel->getActiveSheet()->setCellValue('F'.$count, $dt->id_parent);
            
            $count++; 
            $no++;
      	}
            
        foreach(range('A','F') as $columnID) {
        		
        	$this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        
		/*
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //$this->excel->getActiveSheet()->mergeCells('A1:D1');
        */
        
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
             
        $filename='marina_category.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        $objWriter->save('php://output');
       
        
        $this->template
            ->append_js('module::jquery.ui.datepicker.js')
			->title('Marina Export Menu')
			->set('url', $form_action)
			->build('admin/category/export_form');
	}
}