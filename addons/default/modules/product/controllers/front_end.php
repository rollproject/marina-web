<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
class Front_end extends Public_Controller {
	
	public	$validation_rules = array(
		 
	 	'complate_order' => array(
		 		
	 		array(
					
				'field' => 'name_customer',
				'label' => 'Name Customer',
				'rules' => 'trim|required'
			),
			
			array(
			
				'field' => 'email_customer',
				'label' => 'Email',
				'rules' => 'trim|required'
			),
			
			array(
				
				'field' => 'phone_customer',
				'label' => 'Phone',
				'rules' => 'trim|required'
			),
				
			array(
			
				'field' => 'payment_method',
				'label' => 'Payment',
				'rules' => 'trim|required'
			),
			
			array(
			
				'field' => 'service_method',
				'label' => 'Service',
				'rules' => 'trim|required'
			),
			
			array(
			
				'field' => 'note',
				'label' => 'Note',
				'rules' => ''
			),
				
			array(
				
				'field' =>'recaptcha_challenge_field',
				'label' => 'Security Code',
				'rules' => 'trim|required|callback__recaptcha_check'
			)
		),
		
		'catering_order' => array(
		 		
	 		array(
					
				'field' => 'name_customer',
				'label' => 'Name Customer',
				'rules' => 'trim|required'
			),
			
			array(
			
				'field' => 'email_customer',
				'label' => 'Email',
				'rules' => 'trim|required'
			),
			
			array(
				
				'field' => 'phone_customer',
				'label' => 'Phone',
				'rules' => 'trim|required'
			),
				
			array(
			
				'field' => 'payment_method',
				'label' => 'Payment',
				'rules' => 'trim|required'
			),
			
			array(
			
				'field' => 'service_method',
				'label' => 'Service',
				'rules' => 'trim|required'
			),
			
			array(
			
				'field' => 'note',
				'label' => 'Note',
				'rules' => ''
			),
				
			array(
				
				'field' =>'recaptcha_challenge_field',
				'label' => 'Security Code',
				'rules' => 'trim|required|callback__recaptcha_check'
			)
		)
	);
	
	public function __construct() {
			
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model('product_m');
		$this->load->helper('date');
		$this->template->set_layout('product.html');
		
		$this->load->library(array('recaptcha','form_validation'));
		$this->template->set('recaptcha_html',$this->recaptcha->recaptcha_get_html());
		
	}

	public function index() {
		
		$this->get_category();
	}
	
	public function get_category() {
		$title		= 'Marina Resto Menus';
		$category 	= $this->product_m->get_all_category('product_category', 'id_parent', 0);	
		
		$this->template
			 ->title($title)
			 ->set('category', $category)
			 ->build('menu');
	}
	
	public function get_category_by_id($id_category) {
		
		$title		= 'Marina Resto Menus';
		$url		= site_url().'/product/front_end/get_category';
		$category 	= $this->product_m->get_all_category('product_category', 'id_parent', $id_category);
		$item		= $this->product_m->get_item_by_category($id_category);
		
		$cat		= $this->product_m->get_all_category('product_category', 'id_category', $id_category);
		foreach ($cat as $value) {
			
			$judul	= $value->name_category;
			$desc	= $value->description;
			
		}
		
		$sid 	= session_id();
		$jml	= $this->product_m->get_order($sid);
		
		foreach ($jml as $value) {
			
			$jml  	= $value->jumlah;
			$total	= $value->total;
		}
		
		//var_dump($cat); die();
		$this->template
			 ->title($title)
			 ->set('item', $item)
			 ->set('category', $category)
			 ->set('url', $url)
			 ->set('judul', $judul)
			 ->set('jml', $jml)
			 ->set('total', $total)
			 ->set('desc', $desc)			 
			 ->build('menu_sub');
		
	}
	
	public function add_cart() {
		
		$sid 	= session_id();
		$title	= 'Marina Resto';
		
		$this->form_validation->set_rules('qty', 'Quantity', 'required|numeric');
		if ($this->form_validation->run() == TRUE) {
			
			if($this->input->post('qty') == 0) {
				
				$order_temp = array(
				
					'id_session'	 => $sid,
					'id_item'	 	 => $this->input->post('id_item'),
					'price_per_item' => $this->input->post('price_per_item'),
					'qty'		 	 => 1	
				);
				
				
				$id_choice = $this->input->post('id_choice');
				if($id_choice) {
						
					$list = $this->input->post('list_choice');
					//var_dump($list); die();
					$arr_choice = array();
				
					
					foreach($id_choice as $k=>$kk){
						if(isset($list[$kk])) {
							$arr_choice[$kk] = $list[$kk]; 
						} 
					}
					
					$order_temp['choice'] = serialize($arr_choice);
					
				}
				$this->product_m->insert_('product_temp_order', $order_temp);
				redirect(site_url().'/product/front_end/view_cart');
				
			}else {
				
				$order_temp = array(
				
					'id_session'	 => $sid,
					'id_item'	 	 => $this->input->post('id_item'),
					'price_per_item' => $this->input->post('price_per_item'),
					'qty'		 	 => $this->input->post('qty')	
				);
				
				
				$id_choice = $this->input->post('id_choice');
				if($id_choice) {
						
					$list = $this->input->post('list_choice');
					//var_dump($list); die();
					$arr_choice = array();
				
					
					foreach($id_choice as $k=>$kk){
						if(isset($list[$kk])) {
							$arr_choice[$kk] = $list[$kk]; 
						} 
					}
					
					$order_temp['choice'] = serialize($arr_choice);
					
				}
				$this->product_m->insert_('product_temp_order', $order_temp);
				redirect(site_url().'/product/front_end/view_cart');
			}
			
		}else {
			
			$this->session->set_flashdata('error', 'Quantity must be number');
			redirect(site_url().'product/front_end/get_category');	
		}	
	}
	
	/*public function add_cart() {
		
		$sid 	= session_id();
		$title	= 'Marina Resto';
		$sid 	= session_id();
		$item	= $this->product_m->get_item_cart($sid);
		//$jml	= $this->product_m->get_count_cart($sid);
		
		//cart 
		if(count($item) > 0) {
			
			foreach($item as $carts) {
			
				$id_order_	= $carts->id_temp_order;
				$id_item_	= $carts->id_item;
				$choice_	= $carts->choice;
				$qty_		= $carts->qty;
			}	
		}else {
			
			$id_order_	= 0;
			$id_item_	= 0;
			$choice_	= 0;
			$qty_		= 0;
		}
		
		$this->form_validation->set_rules('qty', 'Quantity', 'required|numeric');
		if ($this->form_validation->run() == TRUE) {
			
			if($this->input->post('qty') == 0) {
				
				$order_temp = array(
				
					'id_session'	 => $sid,
					'id_item'	 	 => $this->input->post('id_item'),
					'price_per_item' => $this->input->post('price_per_item'),
					'qty'		 	 => 1	
				);
				
				
				$id_choice = $this->input->post('id_choice');
				if($id_choice) {
						
					$list = $this->input->post('list_choice');
					//var_dump($list); die();
					$arr_choice = array();
				
					
					foreach($id_choice as $k=>$kk){
						if(isset($list[$kk])) {
							$arr_choice[$kk] = $list[$kk]; 
						} 
					}
					
					$order_temp['choice'] = serialize($arr_choice);
					
				}
				
				var_dump($order_temp); die();
				$this->product_m->insert_('product_temp_order', $order_temp);
				redirect(site_url().'/product/front_end/view_cart');
				
			}else {
				
				$order_temp = array(
				
					'id_session'	 => $sid,
					'id_item'	 	 => $this->input->post('id_item'),
					'price_per_item' => $this->input->post('price_per_item'),
					'qty'		 	 => $this->input->post('qty')	
				);
				
				//choice
				$id_choice = $this->input->post('id_choice');
				if($id_choice) {
						
					$list = $this->input->post('list_choice');
					//var_dump($list); die();
					$arr_choice = array();
				
					
					foreach($id_choice as $k=>$kk){
						if(isset($list[$kk])) {
							$arr_choice[$kk] = $list[$kk]; 
						} 
					}
					
					$order_temp['choice'] = serialize($arr_choice);
					
				}
				
				if($id_item_ == $this->input->post('id_item') ) {
						
					if($id_item_ == $this->input->post('id_item') && $order_temp['choice'] == ''){
							
						$qtys	= $qty_ + $this->input->post('qty');
						$query = array(
					
							'qty'		 	 => $qtys	
						);
						
						$this->product_m->update_('id_temp_order', $id_order_, 'product_temp_order', $query);
						$this->session->set_flashdata('succes', 'Your qty order has been update');
						redirect(site_url().'/product/front_end/view_cart');
						
					}else if($id_item_ == $this->input->post('id_item') && $choice_ == $order_temp['choice']){
							
						$qtys	= $qty_ + $this->input->post('qty');
						$query = array(
					
							'qty'		 	 => $qtys	
						);
						
						$this->product_m->update_('id_temp_order', $id_order_, 'product_temp_order', $query);
						$this->session->set_flashdata('succes', 'Your qty order has been update');
						redirect(site_url().'/product/front_end/view_cart');
					}else{
						$this->product_m->insert_('product_temp_order', $order_temp);
						$this->session->set_flashdata('succes', 'succes add item to your cart ');
						redirect(site_url().'/product/front_end/view_cart');
					}
					/*
					if($choice_ == $order_temp['choice']) {
					
						$qtys	= $qty_ + $this->input->post('qty');
						$query = array(
					
							'qty'		 	 => $qtys	
						);
						
						//var_dump($qtys);
						//echo 'cart update'; die();
						$this->product_m->update_('id_temp_order', $id_order_, 'product_temp_order', $query);
						$this->session->set_flashdata('succes', 'Your qty order has been update');
						redirect(site_url().'/product/front_end/view_cart');
							
					}else {
						
						if($order_temp['choice']) {		
							$qtys	= $qty_ + $this->input->post('qty');
							$query = array(
						
								'qty'		 	 => $qtys	
							);
						}
						
						$this->product_m->insert_('product_temp_order', $order_temp);
						$this->session->set_flashdata('succes', 'succes add item to your cart ');
						redirect(site_url().'/product/front_end/view_cart');
					}	
				}else {
					
					//echo 'cart insert'; die();
					$this->product_m->insert_('product_temp_order', $order_temp);
					$this->session->set_flashdata('succes', 'succes add item to your cart ');
					redirect(site_url().'/product/front_end/view_cart');
				}				
			}
			
		}else {
			
			$this->session->set_flashdata('error', 'Quantity must be number');
			redirect(site_url().'/product/front_end/get_category');	
		}	
	}*/
	
	public function view_cart() {
		
		$title	= 'Marina Resto';
		$judul	= 'Review your Order';
		$sid 	= session_id();
		$item	= $this->product_m->get_item_cart($sid);
		$jml	= $this->product_m->get_count_cart($sid);
		$jumlah	= $jml[0]->total;
		
		$arr_item_detail = array();		
		foreach ($item as $key => $val) {
			
			if($val->choice != '') {
				
				$choice_ 	= unserialize($val->choice);
				
				foreach($choice_ as $t=>$tt){
					$arr_choice[] = $t;	
					$choice 	= $this->db->get_where('default_product_choice', array('id_choice'=>$t));
					//var_dump($choice->row('name_choice')); die();
					$arr_item_detail[$key][$t]['name'] = $choice->row('name_choice'); 
					
					$choice_kid	= $this->db->get_where('default_product_list_choice', array('id_list'=>$tt));
					$arr_item_detail[$key][$t]['kid'] = $choice_kid->row('name_list_choice');
				}
					
				
					/*
				$this->db->where_in('id_choice', $arr_choice);
				$choice 	= $this->product_m->get_cart_choice($val->id_item);
				//$arr_item_detail[$key][]
				
				$this->db->where_in('id_list', $choice_);
				$kid_choice = $this->db->get_where('default_product_list_choice');	
				//var_dump($kid_choice ->result_array());	
				*/
			}
		}
		
		//die();
		$this->template
			 ->title($title)
			 ->set('item', $item)
			 ->set('choice', $arr_item_detail)
			 ->set('judul', $judul)
			 ->set('jml', $jumlah)
			 ->build('cart');
	}
	
	function detail_item($id_item) {
		
		$title	= 'Marina Resto';
		$judul	= 'Add item to cart';
		$item	= $this->product_m->get_item_by_id($id_item);
		$choice	= $this->product_m->get_choice_by_item($id_item);
		$master_choice 	= array();
		$data_choice 	= array();
		foreach($choice as $key => $choices) {
				
			//var_dump($choices);
			$master_choice[$choices->id_choice] = $choices->name_choice;
			$id_choice = $choices->id_choice;	
			
			$kid_choice = $this->product_m->get_kid_choice($id_choice);
			foreach($kid_choice as $key2 => $val) {
				
				$data_choice[$id_choice][$val->id_list] = $val->name_list_choice;
			}	
		}
		
		$sid	= session_id();
		$jml	= $this->product_m->get_count_cart($sid);
		$jumlah	= $jml[0]->total;
		
		$this->template
			 ->title($title)
			 ->set('item', $item)
			 ->set('judul', $judul)
			 ->set('jml', $jumlah)
			 ->set('choice', $data_choice)
			 ->set('master', $master_choice)
			 ->build('detail_item');
	}
	
	function delete_order($id_temp) {
		
		$this->product_m->delete_('product_temp_order', 'id_temp_order', $id_temp);
		$this->session->set_flashdata('error', 'Your oder has delete from cart');
		redirect(site_url().'/product/front_end/view_cart');
	}
	
	function update_order() {
		
		$id = $this->input->post('id_temp_order');
		$this->form_validation->set_rules('qty', 'Quantity', 'required|numeric');
		
		if ($this->form_validation->run() == TRUE) {
			
			$order_temp = array(
				
				'qty'	=> $this->input->post('qty')
			);
			
			$this->product_m->update_('id_temp_order', $id, 'product_temp_order', $order_temp);
			$this->session->set_flashdata('succes', 'Your order has been update');
			redirect(site_url().'/product/front_end/view_cart');
		
		}else {
			
			$this->session->set_flashdata('error', 'Quantity must be number and can not be null');
			redirect(site_url().'/product/front_end/view_cart');
		}
	}
	
	function finish_order() {
		
		$title	= 'Marina Resto';
		$judul	= 'Total Order';
		$sid	= session_id();
		$tgl	= date("Ymd");
		$jam 	= date("H:i:s");
		$isi	= array();
		$cart	= $this->product_m->cart($sid);

		
		$order 	= array(
			
			'date_order' => $tgl
		);
		
		$id_order	= $this->product_m->insert_to_order('product_order', $order);
		$this->session->set_userdata('id_order', $id_order);
		foreach ($cart as $key => $val) {
			
			$isi[] = $val; 
			
			$this->product_m->insert_('product_list_order', array(
					
					'id_order'		 => $id_order,
					'id_item'		 => $val->id_item,
					'price_per_item' => $val->price_per_item,
					'qty'			 => $val->qty,
					'choice_method'	 => $val->choice
			));
		}
		
		redirect(site_url().'/product/front_end/complate_order');	
	}
	
	function complate_order() {
		
		$title	= 'Marina Resto';
		$judul	= 'Complate Order';
		$sid	= session_id();
		$url	= site_url().'/product/front_end/complate_order_process';
		
		$cart_order = $this->product_m->get_order($sid);
		
		$this->template
			 ->title($title)
			 ->set_metadata('og:type', 'Contact us', 'og')
			 ->set_metadata('og:url', current_url(), 'og')
			 ->set_metadata('og:title','Marina Complate Order', 'og')
			 ->set_metadata('og:site_name', Settings::get('site_name'), 'og')
			 ->set_metadata('og:description', 'Marina Compalate order', 'og')
			 ->set_metadata('description','Marina Complate Order')
			 ->set_metadata('keywords', 'marina, marina resto, balinese food, bali resto')
			 ->set('judul', $judul)
			 ->set('item', $cart_order)
			 ->set('url', $url)
			 ->build('finish_order');	
	}
	
	function complate_order_process() {
		
		
		$title	= 'Marina Resto';
		$judul	= 'Complate Order';
		$sid	= session_id();
		$url	= site_url().'/product/front_end/complate_order_process';
		
		/*
		$this->form_validation->set_rules('name_customer', 'Name Customer', 'trim|required');
		$this->form_validation->set_rules('email_customer', 'Email', 'trim|required');
		$this->form_validation->set_rules('address_customer', 'Address', 'trim|required');
		$this->form_validation->set_rules('phone_customer', 'Phone', 'trim|required');
		$this->form_validation->set_rules('payment_method', 'payment', 'trim|required');
		$this->form_validation->set_rules('service_method', 'Service', 'trim|required');
		$this->form_validation->set_rules('recaptcha_challenge_field', 'Security Code', 'trim|required|callback__recaptcha_check');
		*/
		
		//$data_sample = new stdClass;
			
		$this->form_validation->set_rules($this->validation_rules['complate_order']);
		
		if ($this->form_validation->run() == TRUE) {
			
			$order = array(
				
				'name_customer' 	=> $this->input->post('name_customer'),
				'email_customer' 	=> $this->input->post('email_customer'),
				'address_customer' 	=> $this->input->post('address_customer'),
				'phone_customer' 	=> $this->input->post('phone_customer'),
				'payment_method' 	=> $this->input->post('payment_method'),
				'service_method' 	=> $this->input->post('service_method'),
				'status_order'		=> 'New Oder',
				'total'				=> $this->input->post('total'),
				'note' 				=> $this->input->post('note'),
			);
			
			$this->product_m->update_('id_order', $this->session->userdata('id_order'), 'product_order', $order);
			
			//delete order temp
			$this->product_m->delete_('product_temp_order', 'id_session', $sid);
			redirect(site_url().'/product/front_end/detail_finish_order');
			
			//send email
			/*$data_email 		= (array) $this->input->post();
			$data_email['slug'] ='ask-flybest';
			$data_email['from'] = $data_email['email'];
			
			if(!$this->send_email($data_email)) {
					
				$this->template->set('messages',array('error'=>'Cannot Send Email at The Moment, please try again later.'));
			
			}else {
					
				$this->template->set('messages',array('success'=>'Success Send Email '));
				redirect(site_url().'/product/front_end/detail_finish_order');
			}*/
			
		 }else {
			
			redirect(site_url().'/product/front_end/complate_order');	
		}
		 
		foreach( $this->validation_rules['complate_order'] as $field) {
			
			$data_sample->{$field['field']} = $this->input->post($field['field']);
		} 
		
		$this->template
			 ->title($title)
			 ->set_metadata('og:type', 'Contact us', 'og')
			 ->set_metadata('og:url', current_url(), 'og')
			 ->set_metadata('og:title','Marina Complate Order', 'og')
			 ->set_metadata('og:site_name', Settings::get('site_name'), 'og')
			 ->set_metadata('og:description', 'Marina Compalate order', 'og')
			 ->set_metadata('description','Marina Complate Order')
			 ->set_metadata('keywords', 'marina, marina resto, balinese food, bali resto')
			 ->set('judul', $judul)
			 ->set('item', $cart_order)
			 ->set('url', $url)
			 ->build('finish_order');
	}

	function detail_finish_order() {
		
			
		$title	= 'Marina Resto';
		$judul	= 'Your Order';
		
		$order		= $this->product_m->get_by_id_('product_order', 'id_order', $this->session->userdata('id_order'));
		$list_order	= $this->product_m->get_by_id_result('product_list_order', 'id_order', $this->session->userdata('id_order'));	
		
		$this->template
			 ->title($title)
			 ->set('judul', $judul)
			 ->set('order', $order)
			 ->set('list_order', $list_order)
			 ->build('detail_order');			
			
	}

	function complate_order_finish() {
		
			
		$title	= 'Marina Resto';
		$this->session->unset_userdata('id_order');
		redirect(site_url());
			
	}

	function get_catering() {
		
		$sid 		= session_id();
		$title		= 'Marina Resto Catering';
		$catering 	= $this->product_m->get_all_('product_catering', 'name_catering');
		
		$jml	= $this->product_m->get_count_cart_catering($sid);
		
		foreach ($jml as $value) {
			
			if($value != '') {
				
				$jml  	= $value->jumlah;
				$total	= $value->total;	
			
			}else {
				
				$jml  	= 0;
				$total	= 0;
			}
			
		}
		
		$this->template
			 ->title($title)
			 ->set('catering', $catering)
			 ->set('judul', $title)
			 ->set('jml', $jml)
			 ->set('total', $total)
			 ->build('catering');
	}

	function add_catering () {
		
		$sid 	= session_id();
		$title	= 'Marina Resto Catering';
		
		$this->form_validation->set_rules('qty', 'Quantity', 'required|numeric');
		if ($this->form_validation->run() == TRUE) {
			
			$order_temp = array(
					
				'id_session'  => $sid,
				'id_catering' => $this->input->post('id_catering'),
				'price' 	  => $this->input->post('price'),
				'qty'		  => $this->input->post('qty')	
			);
				
			$this->product_m->insert_('product_temp_catering', $order_temp);
			redirect(site_url().'/product/front_end/view_cart_catering');
		}else {
			
			$this->session->set_flashdata('error', 'Quantity must be number');
			redirect(site_url().'product/front_end/get_catering');	
		}
	}
	
	public function view_cart_catering() {
		
		$title	= 'Marina Resto';
		$judul	= 'Review your Order';
		$sid 	= session_id();
		$item	= $this->product_m->get_catering_cart($sid);
		$jml	= $this->product_m->get_count_cart_catering($sid);
		
		foreach ($jml as $value) {
			
			$jml  	= $value->jumlah;
			$total	= $value->total;
		}
		
		$this->template
			 ->title($title)
			 ->set('item', $item)
			 ->set('judul', $judul)
			 ->set('jml', $jml)
			 ->set('total', $total)
			 ->build('cart_catering');
	}
	
	function delete_temp_catering($id_temp) {
		
		$this->product_m->delete_('product_temp_catering', 'id_temp_order', $id_temp);
		$this->session->set_flashdata('succes', 'Your oder has delete from cart');
		redirect(site_url().'/product/front_end/view_cart_catering');
	}

	function update_temp_catering() {
		
		$id = $this->input->post('id_temp_order');
		$this->form_validation->set_rules('qty', 'Quantity', 'required|numeric');
		
		if ($this->form_validation->run() == TRUE) {
			
			$order_temp = array(
				
				'qty'	=> $this->input->post('qty')
			);
			
			$this->product_m->update_('id_temp_order', $id, 'product_temp_catering', $order_temp);
			$this->session->set_flashdata('succes', 'Your order has been update');
			redirect(site_url().'/product/front_end/view_cart_catering');
		
		}else {
			
			$this->session->set_flashdata('error', 'Quantity must be number and can not null');
			redirect(site_url().'/product/front_end/view_cart_catering');
		}
	}
	
	function complate_order_catering() {
			
		$title	= 'Marina Resto';
		$judul	= 'Review your Order';
		$sid 	= session_id();
		$item	= $this->product_m->get_catering_cart($sid);
		$jml	= $this->product_m->get_count_cart_catering($sid);
		$url	= site_url().'/product/front_end/complate_catering_process';
		
		foreach ($jml as $value) {
			
			$jml  	= $value->jumlah;
			$total	= $value->total;
		}
		//$this->product_m->delete_('product_temp_catering', 'id_session', $sid);
		
		//var_dump($item); die();
		$this->template
			 ->title($title)
			 ->set_metadata('og:type', 'Marina Catering', 'og')
			 ->set_metadata('og:url', current_url(), 'og')
			 ->set_metadata('og:title','Marina Complate Order', 'og')
			 ->set_metadata('og:site_name', Settings::get('site_name'), 'og')
			 ->set_metadata('og:description', 'Marina Compalate order', 'og')
			 ->set_metadata('description','Marina Complate Order')
			 ->set_metadata('keywords', 'marina, marina resto, balinese food, bali resto')
			 ->set('item', $item)
			 ->set('judul', $judul)
			 ->set('jml', $jml)
			 ->set('total', $total)
			 ->set('url', $url)
			 ->build('finish_catering');
	}

	function complate_catering_process() {
			
		$title	= 'Marina Resto';
		$judul	= 'Review your Order';
		$sid 	= session_id();
		$url	= site_url().'/product/front_end/complate_catering_process';
		
		$this->form_validation->set_rules($this->validation_rules['complate_order']);
		
		if ($this->form_validation->run() == TRUE) {
			
			//send email
			/*$data_email 		= (array) $this->input->post();
			$data_email['slug'] ='ask-flybest';
			$data_email['from'] = $data_email['email'];
			
			if(!$this->send_email($data_email)) {
					
				$this->template->set('messages',array('error'=>'Cannot Send Email at The Moment, please try again later.'));
			
			}else {
					
				$this->template->set('messages',array('success'=>'Success Send Email '));
				redirect(site_url().'/product/front_end/detail_finish_order');
			}*/
			
			$this->product_m->delete_('product_temp_catering', 'id_session', $sid);
			redirect(site_url().'home');
			
		 }else {
			
			redirect(site_url().'/product/front_end/complate_order_catering');	
		}
		 
		foreach( $this->validation_rules['complate_order'] as $field) {
			
			$data_sample->{$field['field']} = $this->input->post($field['field']);
		} 
		
		$this->template
			 ->title($title)
			 ->set_metadata('og:type', 'Marina Catering', 'og')
			 ->set_metadata('og:url', current_url(), 'og')
			 ->set_metadata('og:title','Marina Complate Order', 'og')
			 ->set_metadata('og:site_name', Settings::get('site_name'), 'og')
			 ->set_metadata('og:description', 'Marina Compalate order', 'og')
			 ->set_metadata('description','Marina Complate Order')
			 ->set_metadata('keywords', 'marina, marina resto, balinese food, bali resto')
			 ->set('judul', $judul)
			 ->set('item', $cart_order)
			 ->set('url', $url)
			 ->build('finish_order');		
	}

	function _recaptcha_check($value) {
		
		$response = $this->recaptcha->recaptcha_check_answer ($_SERVER['REMOTE_ADDR'],  $this->input->post('recaptcha_challenge_field'),  		$this->input->post('recaptcha_response_field'));
		
		if(!$this->recaptcha->is_valid){
			//$this->recaptcha->error
			$this->form_validation->set_message('_recaptcha_check','Incorrect Recaptcha Value');
			return false;
		
		}else{
		
			return true;
		}
	}
	
	private function send_email($data = array()) {
	     
	
		$slug = $data['slug'];
	    unset($data['slug']);
	
	    $this->load->model('templates/email_templates_m');
	
		//get all email templates
		$templates = $this->email_templates_m->get_templates($slug);
	
	    //make sure we have something to work with
	    if ( ! empty($templates)) {
	    	
			$lang	   = isset($data['lang']) ? $data['lang'] : Settings::get('site_lang');
			$from	   = isset($data['from']) ? $data['from'] : Settings::get('server_email');
	        $from_name = isset($data['name']) ? $data['name'] : null;
			$reply_to  = isset($data['reply-to']) ? $data['reply-to'] : $from;
			$to		   = isset($data['to']) ? $data['to'] : Settings::get('contact_email');
	
	        // perhaps they've passed a pipe separated string, let's switch it to commas for CodeIgniter
	        if ( ! is_array($to)) $to = str_replace('|', ',', $to);
	
	       	 	$subject = array_key_exists($lang, $templates) ? $templates[$lang]->subject : $templates['en']->subject ;
	        	$subject = $this->parser->parse_string($subject, $data, true);
	
	            $body = array_key_exists($lang, $templates) ? $templates[$lang]->body : $templates['en']->body ;
	            $body = $this->parser->parse_string($body, $data, true);
		
	            $this->email->from($from, $from_name);
	            $this->email->reply_to($reply_to);
	            $this->email->to($to);
	            $this->email->subject($subject);
	            $this->email->message($body);
				
		
				// To send attachments simply pass an array of file paths in Events::trigger('email')
				// $data['attach'][] = /path/to/file.jpg
				// $data['attach'][] = /path/to/file.zip
				if (isset($data['attach']))
				{
					foreach ($data['attach'] AS $attachment)
					{
						$this->email->attach($attachment);
					}
				}
	
				return (bool) $this->email->send();
	        }
	
	        //return false if we can't find the necessary templates
	        return false;
	    }
}