<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
class Admin extends Admin_Controller
{
	protected $section = 'product';
	
	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model('product_m');

		$this->load->helper('date');

	}
	
	public function index() {
		
		$title		= 'Order';
		$total_rows = $this->product_m->count_all_('product_order');
		$pagination = create_pagination('admin/admin/index', $total_rows);
		$order 		= $this->product_m
					  ->limit($pagination['limit'], $pagination['offset'])->get_all_sort('product_order', 'id_order', 'DESC');
		
		$this->template
			 ->title($title)
			 ->set('judul', $title)
			 ->set('order', $order)
			 ->build('admin/product/product');		
	}
	
	public function detail($id_order) {
		
		$title		= 'Detail Order';
		$order		= $this->product_m->get_detail_order($id_order);
		$list_order	= $this->product_m->get_list_order($id_order);
		
		$this->template
			 ->title($title)
			 ->set('judul', $title)
			 ->set('order', $order)
			 ->set('list_order', $list_order)
			 ->build('admin/product/form');
	}
}