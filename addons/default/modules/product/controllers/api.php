<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
class Api extends Public_Controller {
	
	public function __construct() {
		
		parent::__construct();

		$this->load->model('product_m');
		

		header('Access-Control-Allow-Origin: *');
	}

	public function index() {
		
		
	}
	
	public function get_category() {
		
		$category 	= $this->product_m->get_all_category('product_category', 'id_parent', 0);	
		
		echo json_encode($category);	
	}

	public function get_category_by_id($id_category) {
		
		$category 	= $this->product_m->get_all_category('product_category', 'id_parent', $id_category);
		$item		= $this->product_m->get_item_by_category($id_category);
		
		foreach ($item as &$val) {
			$choice		= $this->product_m->get_choice_by_item($val->id_item);
			$val->choice = array();
			//$val->choice = $choice;
			$m_id = 0;
			foreach($choice as $key => $choices) {
					
				//var_dump($choices);
				//$master_choice[$choices->id_choice] = $choices->name_choice;
				//$id_choice = $choices->id_choice;	
				$val->choice[$m_id] = new stdClass();
				$val->choice[$m_id]->id = $choices->id_choice; 
				$val->choice[$m_id]->name = $choices->name_choice; 
				$val->choice[$m_id]->child = array();
				$c_id = 0;
				$kid_choice = $this->product_m->get_kid_choice( $choices->id_choice);
				
				foreach($kid_choice as $key2 => $v) {
					//$data_choice[$id_choice][$val->id_list] = $val->name_list_choice;
					$val->choice[$m_id]->child[$c_id] = new stdClass();
					$val->choice[$m_id]->child[$c_id]->id = $v->id_list;
					$val->choice[$m_id]->child[$c_id]->name = $v->name_list_choice;
					$c_id++;
				}	
				$m_id++;
			}	
		}
		
		
		$cat		= $this->product_m->get_all_category('product_category', 'id_category', $id_category);
		foreach ($cat as $value) {
			
			$judul	= $value->name_category;
			$desc	= $value->description;
			
		}
		$result = array_merge($category, $item);
		echo json_encode($result);
		//echo json_encode($category);
		//echo json_encode($item);
		//echo json_encode($cat);
	}
	
	function detail_item($id_item) {
		
		$item		= $this->product_m->get_item_by_id($id_item);
		$choice		= $this->product_m->get_choice_by_item($id_item);
		$master_choice 	= array();
		$data_choice 	= array();
		$item =$item[0];
		$item->choice = array();
		$m_id = 0;
		foreach($choice as $key => $choices) {
				
			//var_dump($choices);
			//$master_choice[$choices->id_choice] = $choices->name_choice;
			//$id_choice = $choices->id_choice;	
			$item->choice[$m_id] = new stdClass();
			$item->choice[$m_id]->id = $choices->id_choice; 
			$item->choice[$m_id]->name = $choices->name_choice; 
			$item->choice[$m_id]->child = array();
			$c_id = 0;
			$kid_choice = $this->product_m->get_kid_choice( $choices->id_choice);
			
			foreach($kid_choice as $key2 => $val) {
				//$data_choice[$id_choice][$val->id_list] = $val->name_list_choice;
				$item->choice[$m_id]->child[$c_id] = new stdClass();
				$item->choice[$m_id]->child[$c_id]->id = $val->id_list;
				$item->choice[$m_id]->child[$c_id]->name = $val->name_list_choice;
				$c_id++;
			}	
			$m_id++;
		}
		var_dump($item);
		//$result = array_merge($item, $master_choice, $data_choice);
		echo json_encode($item);
	}
	
	/*public function add_carts() {	
		$temp	= array(

			'id_session'	=> $this->input->post('id_session'),
			'id_item'		=> $this->input->post('id_item'),
			'price_per_item'=> $this->input->post('price_per_item'),
			'qty'			=> $this->input->post('qty'),
			'choice'		=> $this->input->post('choice')					
		);			
		if($this->product_m->insert_('product_temp_order', $temp)){
			echo 'berhasil';
		}
		
	}*/
	
	public function add_carts() {
		
		$if_exist = $this->product_m->get_if_menu_exist('product_temp_order',
								$this->input->post('id_session'),
								$this->input->post('id_item'));
		
		//$id_temp_order = null;
		/*foreach($if_exist as $data){
			$id_temp_order = $data->id_temp_order;
		}*/
		
		$temp	= array(
				'id_session'	=> $this->input->post('id_session'),
				'id_item'		=> $this->input->post('id_item'),
				'price_per_item'=> $this->input->post('price_per_item'),
				'qty'			=> $this->input->post('qty'),
				'choice'		=> $this->input->post('choice')					
			);
		
		if($if_exist){
			//sekalian update lansung return
			$temp['qty'] = $this->input->post('qty')+$if_exist[0]->qty;
			if($this->product_m->update_menu_if_exist('product_temp_order',
								$if_exist[0]->id_temp_order,
								$temp)){
				echo 'berhasil';
			}
		}else{
			if($this->product_m->insert_('product_temp_order', $temp)){
				echo 'berhasil';
			}
		}
	
	}
	
	
	public function finish_order() {
		
		$sid	= $_POST['token_id'];
		$tgl	= date("Ymd");
		$cart	= $this->product_m->cart($sid);
		$order = array(
			
			'date_order' => $tgl
		);
		
		$id_order	= $this->product_m->insert_to_order('product_order', $order);
		
		$this->session->set_userdata('id_order', $id_order);
		foreach ($cart as $key => $val) {
			
			$isi[] = $val; 
			
			$this->product_m->insert_('product_list_order', array(
					
					'id_order'		 => $id_order,
					'id_item'		 => $val->id_item,
					'price_per_item' => $val->price_per_item,
					'qty'			 => $val->qty,
					'choice_method'	 => $val->choice
			));
		}
		
		echo $id_order;
	}
	
	
	function complate_order_process() {
		
		$sid	= $_POST['token_id'];
		$cart_order = $this->product_m->get_order($sid);
		
		$order = array(
				
			'name_customer' 	=> $this->input->post('name_customer'),
			'email_customer' 	=> $this->input->post('email_customer'),
			'address_customer' 	=> $this->input->post('address_customer'),
			'phone_customer' 	=> $this->input->post('phone_customer'),
			'payment_method' 	=> $this->input->post('payment_method'),
			'service_method' 	=> $this->input->post('service_method'),
			'status_order'		=> 'New Oder',
			'total'				=> $this->input->post('total'),
			'note' 				=> $this->input->post('note'),
		);
		
		$id_ = $_POST['id_order_mobile']; 	
		$this->product_m->update_('id_order', $id_, 'product_order', $order);
		$this->product_m->delete_('product_temp_order', 'id_session', $sid);		
			
	}
	
	
	public function update_qty(){
		$id_session = $this->input->post('id_session');
		$id_temp_order = $this->input->post('id_temp_order');
		$temp = array(
			//'id_session'=> $this->input->post('id_session'),
			'id_session'	=> $id_session,
			'qty'		=> $this->input->post('qty')
		);
		$this->product_m->update_qty_item($id_temp_order,$id_session,'product_temp_order',$temp);
	}
	
	//edited
	/*
	function update_option_choice(){
		
		$if_exist = $this->product_m->get_if_menu_exist('product_temp_order',
								$this->input->post('id_session'),
								$this->input->post('id_item'));
		
		$sid_session	= $this->input->post('id_session');
		$id_item	= $this->input->post('id_item');
		
		$temp	= array(
			
			'id_session'	=> $this->input->post('id_session'),
			'id_item'		=> $this->input->post('id_item'),
			'price_per_item'	=> $this->input->post('price_per_item'),
			'qty'			=> $this->input->post('qty'),
			'choice'		=> $this->input->post('choice')		
				
		);
		
		
		
		$temp_isGotSame = false;
		$tempQty = 0;
		if($if_exist){
			
			
			
			$obj = json_decode($this->input->post('choice'),true);
			
			$index_target = null;
			$tempindex_target = null;
			$kel = 0;
			
			foreach($if_exist as $index_data){
				$kel = 0;
				foreach($obj as $data){
					$tempObjMethod = $data['method_choice'];
					$tempObjChoice = $data['value_choice'];
				//echo var_dump($data['method_choice']);
					foreach($if_exist as $data_if_exist){
						$obj_if_exist =  json_decode($data_if_exist->choice,true);
						if($this->filter_choice_selection($obj_if_exist,$tempObjMethod,$tempObjChoice)){
							$kel += 1;
							echo ' '.$tempObjMethod.' & '.$tempObjChoice;
							$tempQty = $data_if_exist->qty;
						}else{
							
						}
					}
					
					if(count($obj) == $kel){
						echo 'samaaa';
						$index_target = $index_data->id_temp_order;
						break 2;
					}
				}
			}
			
			
			if($index_target != null){
				//echo $tempQty;
				echo  ' '.$index_target;
				$temp['qty'] = $this->input->post('qty')+$tempQty;
				$this->product_m->updateChoiceById_tempOrder($index_target,'product_temp_order', $temp);
				//echo 'berhasil';
			}else{
				if($this->product_m->insert_('product_temp_order', $temp)){
					echo 'berhasil';
				}
			}
		}else{
			$this->product_m->updateMoreParameter($sid_session,$id_item,'product_temp_order', $temp);
			echo 'berhasil';
		}
		
	}*/
	
	
	function isBoolean($value) {
		if ($value && strtolower($value) !== "false") {
		   return true;
		} else {
		   return false;
		}
	}
	
	function update_option_choice(){
		/* isValid berguna
		 * jika true maka yang di ganti pilihan custom choice
		 * kalo false berarti update qty atau input baru
		 */
		$if_change_custom = $this->input->post('isValid');
		
		$if_exist = $this->product_m->get_if_menu_exist('product_temp_order',
								$this->input->post('id_session'),
								$this->input->post('id_item'));
		
		$sid_session	= $this->input->post('id_session');
		$id_item	= $this->input->post('id_item');
		
		$temp	= array(
			
			'id_session'		=> $this->input->post('id_session'),
			'id_item'		=> $this->input->post('id_item'),
			'price_per_item'	=> $this->input->post('price_per_item'),
			'qty'			=> $this->input->post('qty'),
			'choice'		=> $this->input->post('choice')		
				
		);
		
		$last_Qty = 0;
		//echo $tempObjChoice.' == '.$data['value_choice'];
		$temp_isGotSame = false;
		if($if_exist){
			
			
			
			$obj = json_decode($this->input->post('choice'),true);
			
			$index_target = null;
			$tempindex_target = null;
			$kel = 0;
			
			foreach($if_exist as $index_data){
				$kel = 0;
				foreach($obj as $data){
					$tempObjMethod = $data['method_choice'];
					$tempObjChoice = $data['value_choice'];
				//echo var_dump($data['method_choice']);
					foreach($if_exist as $data_if_exist){
						$obj_if_exist =  json_decode($data_if_exist->choice,true);
						
						if($this->filter_choice_selection($obj_if_exist,$tempObjMethod,$tempObjChoice)){
							$kel += 1;
							//echo ' aaaa =>'.$kel.' ';
							
							if(count($obj) == $kel){
								$index_target = $data_if_exist->id_temp_order;
								$last_Qty = $data_if_exist->qty;	
								break 3;
							}
						}else{
							//echo ' bbbb ';
						}
					}	
				}
			}
			
			if(!$this->isBoolean($if_change_custom)){
				if($index_target != null){
					$temp['qty'] = $this->input->post('qty')+$last_Qty;
					$this->product_m->updateChoiceById_tempOrder($index_target,'product_temp_order', $temp);
					echo 'update qty berhasil';
				}else{
					if($this->product_m->insert_('product_temp_order', $temp)){
						echo 'berhasil';
					}
				}
			}else{
				//$this->product_m->updateMoreParameter($sid_session,$id_item,'product_temp_order', $temp);
				$this->product_m->updateMoreParameter($this->input->post('id_temp_order'),'product_temp_order', $temp);
				echo 'berhasil';
			}
			
		}else{
			if(!$this->isBoolean($if_change_custom)){
				if($this->product_m->insert_('product_temp_order', $temp)){
					echo 'berhasil';
				}	
			}else{
				//$this->product_m->updateMoreParameter($sid_session,$id_item,'product_temp_order', $temp);
				$this->product_m->updateMoreParameter($this->input->post('id_temp_order'),'product_temp_order', $temp);
				echo 'berhasil';
			}
			
		}
		
	}
	
	function filter_choice_selection($choice,$tempObjMethod,$tempObjChoice){
		$if_same = false;
		foreach($choice as $data){
			//echo 'hasilnya adalah'.$data['value_choice'].' dan '.$tempObjChoice;
			//echo var_dump($data['value_choice']);
			//if($tempObjMethod == $data['method_choice']){
				if($tempObjChoice == $data['value_choice']){	
					$if_same = true;
					break;
				}else{
					$if_same = false;
				}
			//}
		}
		return $if_same;
	}
	
	function delete_temp_order(){
		$id = $this->input->post('id_token');
		$id_temp_order = $this->input->post('id_temp_order');
		$temporder_food = array(
				'id_session' => $id,
				'id_temp_order' => $id_temp_order
			);
		$this->product_m->delete_food_tempOrder($id,$id_temp_order, 'product_temp_order', $temporder_food);
		echo 'berhasil';
	}
	
	function check_if_allready_exist(){
		$sid	= $_POST['token_id'];
		$cart	= $this->product_m->cart($sid);
		if($cart){
			echo true;
		}else{ 
			echo false;
		}	
	}
	
	function get_list_catering(){
		$catering = $this->product_m->get_all_('product_catering', 'name_catering');	
		echo json_encode($catering);	
	}
	
	function add_cart_catering(){
		$sid 	= $this->input->post('id_token');
		
		$order_temp = array(
			'id_session'  => $sid,
			'id_catering' => $this->input->post('id_catering'),
			'price' 	  => $this->input->post('price'),
			'qty'		  => $this->input->post('qty')	
		);
			
		$this->product_m->insert_('product_temp_catering', $order_temp);
		
	}
	
	function delete_temp_catering(){
		$id = $this->input->post('id_token');
		$id_catering = $this->input->post('id_catering');
		$temporder_catering = array(
				'id_session' => $id,
				'id_catering' => $id_catering
			);
		$this->product_m->delete_Catering_tempOrder($id,$id_catering, 'product_temp_catering', $temporder_catering);
		
	}
	
	function update_temp_catering() {
		
		$id = $this->input->post('id_token');
		$id_catering = $this->input->post('id_catering');
		$temporder_catering = array(
				'id_session' => $id,
				'id_catering' => $id_catering,
				'price' => $this->input->post('price'),
				'qty'		  => $this->input->post('qty')	
			);
		
		
		$this->product_m->update_Catering_tempOrder($id,$id_catering, 'product_temp_catering', $temporder_catering);
		
	}
	
	function get_cart_mobile($id_session) {
		
		$cart	= $this->product_m->get_item_cart_mobile($id_session);
		echo json_encode($cart);
	}
}