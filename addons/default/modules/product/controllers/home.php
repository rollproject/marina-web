<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
class Home extends Public_Controller {
	
	public function __construct() {
			
		parent::__construct();

		$this->load->model('product_m');
		$this->template->set_layout('home.html');
		
	}

	public function index() {
		
		$this->get_home();
	}
	
	function get_home() {
		
		$title		= 'Marina Resto Menus';
		$category 	= $this->product_m->get_all_category('product_category', 'id_parent', 0);	
		
		$this->template
			 ->title($title)
			 ->set('category', $category)
			 ->build('home');
	}
}