<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
class Admin_choice extends Admin_Controller {
	
	public function __construct() {
		
		parent::__construct();

		$this->load->model('product_m');
		$this->load->library('form_validation');
		$this->load->helper('date');
		
	}

	public function index() {
		
		$this->get_all();
	}
	
	//choice
	function get_all() {
		
		$title		 	 = 'Choice Item';
		$url		 	 = site_url().'/product/admin_choice/get_choice';
		$list		 	 = site_url().'/product/admin_choice/get_list';
		$item_and_choice = site_url().'/product/admin_choice/get_item_and_choice';
		$choice		 	 = $this->product_m->get_all_('product_choice', 'name_choice');
		$choice_list 	 = $this->product_m->get_list_choice();
		$item_choice 	 = $this->product_m->get_item_choice();
				
		$this->template
			 ->title($title)
			 ->set('choice', $choice)
			 ->set('url', $url)
			 ->set('list', $list)
			 ->set('choice_list', $choice_list)
			 ->set('item_choice', $item_choice)
			 ->set('item_and_choice', $item_and_choice)
			 ->set('judul', $title)
			 ->build('admin/choice/choice');
	}
	
	function get_choice() {
		
		$title		= 'Choice Item';
		$url		= site_url().'/product/admin_choice/add';
		$choice		= $this->product_m->get_all_('product_choice', 'name_choice');
				
		$this->template
			 ->title($title)
			 ->set('choice', $choice)
			 ->set('url', $url)
			 ->set('judul', $title)
			 ->build('admin/choice/choice_item');
	}
	
	function delete($id_choice) {
	
		$this->product_m->delete_('product_choice', 'id_choice', $id_choice);
		
		redirect(site_url().'/product/admin_choice/get_choice');
	}
	
	function add() {
		
		$title			= "Add Choice Item";
		$form_action	= site_url().'/product/admin_choice/add_process'; 
			
		$this->template
			 ->title($title)
			 ->set('title', $title)
			 ->set('url', $form_action)
			 ->build('admin/choice/form');
		
	}
	
	function add_process() {
	
		$title			= "Add Choice Item";
		$form_action	= site_url().'/product/admin_choice/add_process'; 
		
		$this->form_validation->set_rules('name_choice', 'Choice Name', 'required');
		
		if ($this->form_validation->run() == TRUE) {
			
			$choice = array(
			
				'name_choice' 	=> $this->input->post('name_choice')
			);	
			
			$this->product_m->insert_('product_choice', $choice);			
			redirect(site_url().'/product/admin_choice/get_choice');
		
		}else {
				
			$this->template
			 	 ->title('Add Choice Item')
			  	 ->set('title', $title)
				 ->set('url', $form_action)
			 	 ->build('admin/choice/form');
		}		
	}
	
	function update($id_choice) {
	
		$title			= "Update Choice Item";
		$form_action	= site_url().'/product/admin_choice/update_process';
											
		$choice 		= $this->product_m->get_by_id_('product_choice', 'id_choice', $id_choice);
		$this->session->set_userdata('id_choice', $choice->id_choice);
		
		$name_choice	= $choice->name_choice;
		
		$this->template
			 	 ->title($title)
			  	 ->set('title', $title)
			 	 ->set('url', $form_action)
				 ->set('name_choice', $name_choice)
				 ->build('admin/choice/form');
	}
	
	function update_process() {
	
		$title			= "Update Choice Item";
		$form_action	= site_url().'/product/admin_choice/update_process';
											
		$choice 		= $this->product_m->get_by_id_('product_choice', 'id_choice', $this->session->userdata('id_choice'));
		$this->session->set_userdata('id_choice', $choice->id_choice);
		
		$name_hoice		= $choice->name_choice;
										
		$this->form_validation->set_rules('name_choice', 'Choice Name', 'required');
		
		if ($this->form_validation->run() == TRUE) {
			
			$choice = array(
			
				'name_choice'	=> $this->input->post('name_choice')
			);	
			
			$this->product_m->update_('id_choice', $this->session->userdata('id_choice'), 'product_choice', $choice);
			redirect(site_url().'/product/admin_choice/get_choice');
		
		}else {
			
			$this->template
				 ->title($title)
				 ->set('title', $title)
				 ->set('url', $form_action)
				 ->set('name_choie', $name_choice)
				 ->build('admin/choice/form');
		}
	}
	
	//list hoice
	function get_list() {
		
		$title		 = 'List Choice Item';
		$url		 = site_url().'/product/admin_choice/add_list';
		$choice_list = $this->product_m->get_list_choice();
				
		$this->template
			 ->title($title)
			 ->set('choice_list', $choice_list)
			 ->set('url', $url)
			 ->set('judul', $title)
			 ->build('admin/choice/choice_list_item');
	}
	
	function delete_list($id_list) {
	
		$this->product_m->delete_('product_list_choice', 'id_list', $id_list);
		
		redirect(site_url().'/product/admin_choice/get_list');
	}
	
	function add_list() {
		
		$title			= "Add List Choice Item";
		$form_action	= site_url().'/product/admin_choice/add_list_process'; 
		$choice			= $this->product_m->get_('name_choice', 'product_choice');	
		
		$this->template
			 ->title($title)
			 ->set('title', $title)
			 ->set('choice', $choice)
			 ->set('url', $form_action)
			 ->build('admin/choice/form_list');
		
	}
	
	function add_list_process() {
	
		$title			= "Add List Choice Item";
		$form_action	= site_url().'/product/admin_choice/add_list_process'; 
		$choice			= $this->product_m->get_('name_choice', 'product_choice');	
		
		$this->form_validation->set_rules('id_choice', 'Choice', 'required');
		$this->form_validation->set_rules('name_list_choice', 'Name List Choice', 'required');
		
		if ($this->form_validation->run() == TRUE) {
			
			$list_choice = array(
			
				'id_choice' 		=> $this->input->post('id_choice'),
				'name_list_choice'	=> $this->input->post('name_list_choice')
			);	
			
			$this->product_m->insert_('product_list_choice', $list_choice);			
			redirect(site_url().'/product/admin_choice/get_list');
		
		}else {
				
			
		$this->template
			 ->title($title)
			 ->set('title', $title)
			 ->set('choice', $choice)
			 ->set('url', $form_action)
			 ->build('admin/choice/form_list');
		}		
	}
	
	function update_list($id_list) {
	
		$title			= "Update List Choice Item";
		$form_action	= site_url().'/product/admin_choice/update_list_process';
		$choice			= $this->product_m->get_('name_choice', 'product_choice');									
		$list 			= $this->product_m->get_by_id_('product_list_choice', 'id_list', $id_list);
		$this->session->set_userdata('id_list', $list->id_list);
		
		$name_list_choice	= $list->name_list_choice;
		
		$this->template
			 	 ->title($title)
			  	 ->set('title', $title)
			 	 ->set('url', $form_action)
				 ->set('choice', $choice)
				 ->set('name_list_choice', $name_list_choice)
				 ->build('admin/choice/form_list');
	}
	
	function update_list_process() {
	
		$title			= "Update Choice Item";
		$form_action	= site_url().'/product/admin_choice/update_process';
											
		$choice			= $this->product_m->get_('name_choice', 'product_choice');									
		$list 			= $this->product_m->get_by_id_('product_list_choice', 'id_list', $this->session->userdata('id_list'));
		$this->session->set_userdata('id_list', $list->id_list);
		
		$name_list_choice	= $list->name_list_choice;
										
		$this->form_validation->set_rules('id_choice', 'Choice', 'required');
		$this->form_validation->set_rules('name_list_choice', 'Name List Choice', 'required');
		
		if ($this->form_validation->run() == TRUE) {
			
			$list_choice = array(
			
				'id_choice' 		=> $this->input->post('id_choice'),
				'name_list_choice'	=> $this->input->post('name_list_choice')
			);	
			
			$this->product_m->update_('id_list', $this->session->userdata('id_list'), 'product_list_choice', $list_choice);
			redirect(site_url().'/product/admin_choice/get_list');
		
		}else {
			
			$this->template
			 	 ->title($title)
			  	 ->set('title', $title)
			 	 ->set('url', $form_action)
				 ->set('choice', $choice)
				 ->set('name_list_choice', $name_list_choice)
				 ->build('admin/choice/form_list');
		}
	}

	//item and choice
	function get_item_and_choice() {
		
		$title		 = 'List Choice Item';
		$url		 = site_url().'/product/admin_choice/add_item_choice';
		$item_choice = $this->product_m->get_item_choice();
				
		$this->template
			 ->title($title)
			 ->set('item_choice', $item_choice)
			 ->set('url', $url)
			 ->set('judul', $title)
			 ->build('admin/choice/choice_and_item');
	}
	function delete_master($id_master) {
	
		$this->product_m->delete_('product_master_choice', 'id_master', $id_master);
		
		redirect(site_url().'/product/admin_choice/get_item_and_choice');
	}
	
	function add_item_choice() {
		
		$title			= "Add Item & Choice";
		$form_action	= site_url().'/product/admin_choice/add_item_choice_process'; 
		$choice			= $this->product_m->get_('name_choice', 'product_choice');	
		$item			= $this->product_m->get_all_item();
		
		$this->template
			 ->title($title)
			 ->set('title', $title)
			 ->set('choice', $choice)
			 ->set('item', $item)
			 ->set('url', $form_action)
			 ->build('admin/choice/form_item_choice');
		
	}
	
	function add_item_choice_process() {
	
		$title			= "add Item & Choice";
		$form_action	= site_url().'/product/admin_choice/add_item_choice_process'; 
		$choice			= $this->product_m->get_('name_choice', 'product_choice');	
		$item			= $this->product_m->get_all_item();
		
		$this->form_validation->set_rules('id_choice', 'Choice', 'required');
		$this->form_validation->set_rules('id_item', 'Item', 'required');
		
		if ($this->form_validation->run() == TRUE) {
			
			$master_choice = array(
			
				'id_choice' => $this->input->post('id_choice'),
				'id_item'	=> $this->input->post('id_item')
			);	
			
			$this->product_m->insert_('product_master_choice', $master_choice);			
			redirect(site_url().'/product/admin_choice/get_item_and_choice');
		
		}else {
				
			
		$this->template
			 ->title($title)
			 ->set('title', $title)
			 ->set('choice', $choice)
			 ->set('item', $item)
			 ->set('url', $form_action)
			 ->build('admin/choice/form_item_choice');
		}		
	}
	
	function update_item_choice($id_master) {
	
		$title			= "Update Item & Choice";
		$form_action	= site_url().'/product/admin_choice/update_item_choice_process';
		$choice			= $this->product_m->get_('name_choice', 'product_choice');	
		$item			= $this->product_m->get_all_item();
											
		$master			= $this->product_m->get_by_id('product_master_choice', 'id_master', $id_master);
		$this->session->set_userdata('id_master', $master->id_master);
		$id_choice		= $master->id_choice;
		$id_item		= $master->id_item;
		
		$this->template
			 ->title($title)
			 ->set('title', $title)
			 ->set('url', $form_action)
			 ->set('choice', $choice)
			 ->set('item', $item)
			 ->build('admin/choice/form_item_choice');
	}

	function update_item_choice_process() {
	
		$title			= "Update Item & Choice";
		$form_action	= site_url().'/product/admin_choice/update_item_choice_process';
		$choice			= $this->product_m->get_('name_choice', 'product_choice');	
		$item			= $this->product_m->get_all_item();
											
		$master			= $this->product_m->get_by_id_('product_master_choice', 'id_master', $this->session->userdata('id_master'));
		$this->session->set_userdata('id_master', $master->id_master);
		$id_choice		= $master->id_choice;
		$id_item		= $master->id_item;
											
		$this->form_validation->set_rules('id_choice', 'Choice', 'required');
		$this->form_validation->set_rules('id_item', 'Item', 'required');
		
		if ($this->form_validation->run() == TRUE) {
			
			$master_choice = array(
			
				'id_choice' => $this->input->post('id_choice'),
				'id_item'	=> $this->input->post('id_item')
			);
			
			$this->product_m->update_('id_master', $this->session->userdata('id_master'), 'product_master_choice', $master_choice);
			redirect(site_url().'/product/admin_choice/get_item_and_choice');
		
		}else {
			
			$this->template
			 ->title($title)
			 ->set('title', $title)
			 ->set('url', $form_action)
			 ->set('choice', $choice)
			 ->set('item', $item)
			 ->build('admin/choice/form_item_choice');
		}
	}
}