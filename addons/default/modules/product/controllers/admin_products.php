<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
class Admin_products extends Admin_Controller
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model('products_m');
		

		$this->load->helper('date');
		
	}

	public function index()
	{
		
		
	}


}