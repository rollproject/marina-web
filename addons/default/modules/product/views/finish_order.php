<div id="middle">
	<div class="container">
		<div class="menu-content">
			<div class="page-title">{{ pages:display slug="menu" }} {{custom_fields}} {{page_title}} {{/custom_fields}} {{ /pages:display }} </div>
			<div id="page-banner-wrap">
				{{ pages:display slug="menu" }} {{custom_fields}} {{ image_banner:img }} {{/custom_fields}} {{ /pages:display }}
				<span class="color_1"></span>
				<span class="color_2"></span>
				<span class="color_3"></span>
				<span class="color_4"></span>				
				<div class="clear"></div>
			</div> <!-- #page-banner-wrap -->			
			<div class="page-description">
				{{ pages:display slug="menu" }} {{custom_fields}} {{body}} {{/custom_fields}} {{ /pages:display }} 
				<br /><br />
                <center><a href="" class="button-auto orange">Read our terms & conditions</a></center> 
			</div>
			<div class="general-wp">
              		<?php foreach($item as $val) { ?> 
	              	<div class="total-order side-padding">
	                    <div class="title"><img class="icon" src="{{ theme:image_url file="order-icon.png" }}" /> <?php echo $judul; ?></div>
	                    <div class="note">Note : Please check your order!</div>
	                    <div><label class="tOrange">Total Quantity</label><span class="total-quantity"><?php echo $val->jumlah; ?></span></div>
	                    <div class="dark-line"></div>
	                    <div><label>Total Price</label><span class="total-price">IDR <?php echo $val->total; ?>,00</span></div>
	                    <div class="dark-line"></div>
	              	</div>
              		<div class="form-wp side-padding">
              			<?php echo form_open($url); ?>
              			<div class="half-column">
                            <div class="input-wp">
                                <span>Full Name</span>
                                <input type="text" name="name_customer" />  
                                <?php echo form_error('name_customer'); ?>  
                            </div>
                            <div class="input-wp">
                                <span>Your Email</span>
                                <input type="text" name="email_customer" />
                                <?php echo form_error('email_customer'); ?>
                            </div>
                             <div class="input-wp">
                                <span>Your Address</span>
                                <input type="text" name="address_customer"  />
                                <?php echo form_error('address_customer'); ?>
                            </div>
                            <div class="input-wp">
                                <span>Your Phone Number</span>
                                <input type="text" name="phone_customer" />
                                <?php echo form_error('phone_customer'); ?>
                            </div>
                            <div class="input-wp">
                                <span>Subject / Catering Option</span>
                                <input type="text" name="subject"/>
                                <span class="note" style="color: #000;">Please Note: place your order with minimum 24 hours advance notice. Thank You</span>
                            </div>
                            <div class="input-wp">
                                <span>Pick Up or Delivery Date</span>
                                <input type="text" name="service_method" />
                                <?php echo form_error('service_method'); ?>
                            </div>
                            <div class="input-wp">
                                <span>Payment</span>
                                <input type="text" name="payment_method" />
                                <?php echo form_error('payment_method'); ?>
                                <span class="note" style="color: #000;">Example: Transfer or Cash on Delivery</span>
                            </div>
                            <div class="input-wp">
                                <span>Special Instructions</span>
                                <textarea name="note"></textarea>
                            </div>
                            <!--
                            <div class="input-wp">
                                <span>Please insert code below</span>
                                <span class="captcha">CAPTCHA</span>
                                <input type="text" />
                            </div>
                            -->
                            <div class="input-wp">
			                	<span>Please insert code below</span>
			                	<span class="captcha">CAPTCHA</span>
							 	<?php echo form_error('recaptcha_challenge_field'); ?>	
								<div id="captcha-wrap">
									<?php  echo $recaptcha_html; ?>	
								</div>
							</div>	
                            <div class="input-wp">
                                <!--<span class="note">Please note: all fields are required</span>-->
                                <input type="hidden" value="<?php echo $val->total; ?>" name="total" />
                                <button type="submit" name="submit" id="submit" class="button orange">Send</button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                        <div class="half-column">
                            <span class="orange payment-title">Pembayaran</span>
                            <div class="payment-wp">
                                <div class="left-side"><img src="{{ theme:image_url file="bca.png" }}"/></div>
                                <div class="right-side">
                                    <div class="payment-type">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh</div>
                                    <div class="account-owner">Atas Nama : Account Username</div>
                                    <div class="account-number">No. Rekening : XXXXXXXX</div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="payment-wp">
                                <div class="left-side"><img src="{{ theme:image_url file="mandiri.png" }}"/></div>
                                <div class="right-side">
                                    <div class="payment-type">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh</div>
                                    <div class="account-owner">Atas Nama : Account Username</div>
                                    <div class="account-number">No. Rekening : XXXXXXXX</div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="payment-wp">
                                <div class="left-side"><img src="{{ theme:image_url file="bni.png" }}"/></div>
                                <div class="right-side">
                                    <div class="payment-type">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh</div>
                                    <div class="account-owner">Atas Nama : Account Username</div>
                                    <div class="account-number">No. Rekening : XXXXXXXX</div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            
                            <span class="orange payment-title">Petunjuk Pembayaran</span>
                            <ul class="payment-rule">
                                <li>
                                    Silahkan transfer pembayaran untuk pemesanan anda melalui     rekening Bank yang telah kami sediakan.
                                </li>
                                <li>
                                    Langkah Terakhir Konfirmasikan kembali informasi pembayaran anda melalui :
                                    <ul>
                                        <li>email order@marinarestaurant atau</li>
                                        <li>sms ke 08123456789 (SMS Only) Format SMS : Nomor Order ID, Bank Transfer, Jumlah Transfer, contoh (order id 1200, bank bca, 300.000)</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
              	<?php } ?>
             	<div class="clear"></div>
			</div>
		<!--	
		<div class="menu-mobile-title">Our Services</div>
		{{ widgets:instance id="1"}}
		-->
		<div class="clear"></div>
	</div>
</div>
			