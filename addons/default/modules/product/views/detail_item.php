<div id="middle">
	<div class="container">
		<div class="menu-content">
			<div class="page-title">{{ pages:display slug="menu" }} {{custom_fields}} {{page_title}} {{/custom_fields}} {{ /pages:display }} </div>
			<div id="page-banner-wrap">
				{{ pages:display slug="menu" }} {{custom_fields}} {{ image_banner:img }} {{/custom_fields}} {{ /pages:display }}
				<span class="color_1"></span>
				<span class="color_2"></span>
				<span class="color_3"></span>
				<span class="color_4"></span>				
				<div class="clear"></div>
			</div> <!-- #page-banner-wrap -->			
			<div class="page-description">
				{{ pages:display slug="menu" }} {{custom_fields}} {{body}} {{/custom_fields}} {{ /pages:display }} 
				<br /><br />
                <center><a href="" class="button-auto orange">Read our terms & conditions</a></center> 
			</div>
			<div class="general-wp">
				<p class="side-padding">
                	<span class="tOrange sub-title-2"><?php echo $judul; ?></span><br />
                    <span class="tDark"><i></i></span>
                </p>
                 <div class="cart-box">
                	<div class="left-side">
                    	<span class="side-wrapper"><a href="<?php echo site_url().'/product/front_end/get_category'; ?>">Kembali</a></span>
                    	<div class="clear"></div>
                   	</div>
                    <div class="right-side">
                    	<span class="side-wrapper"><a href="<?php echo site_url().'/product/front_end/view_cart'; ?>" class="view">View Cart</a></span>
                      	<?php if($jml > 0) { ?>
                    		
							<span class="side-wrapper">Food's Cart : <span class="total-cart"><?php echo $jml; ?></span> item's</span>
							
                    	<?php } else { ?> 
                    		
                    		<span class="side-wrapper">Food's Cart : <span class="total-cart">0</span> item's</span>
                    	<?php } ?>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
              	</div>
                <div class="cart-box-mobile">
                	<div class="dark-line"></div>
                    <div class="left-side">
                    	<?php if($jml > 0) { ?>
                    		
							<span class="side-wrapper">Food's Cart : <span class="total-cart"><?php echo $jml; ?></span> item's</span>
							
                    	<?php } else { ?> 
                    		
                    		<span class="side-wrapper">Food's Cart : <span class="total-cart">0</span> item's</span>
                    	<?php } ?>   
                        <div class="clear"></div>
                   	</div>
                    <div class="right-side">
                    	<span class="side-wrapper"><a href="<?php echo site_url().'/product/front_end/view_cart'; ?>" class="view">View Cart</a></span>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
              	</div>
              	<div class="choice">
              		<form method="post" action="<?php echo site_url().'/product/front_end/add_cart'; ?>">
	              		<?php foreach ($item as $value) { ?>
	              			<div class="menu-title"><?php echo $value->name_item; ?></div><br />
	              			<div style="padding: 0 24px 0;"><?php echo $value->description; ?></div>
                            <?php if($master != '') { ?> 
                           		<ul class="main-list">
                            	<?php foreach ($master as $key => $val) { ?>
                            		<li>
	                            		<div class="sub-title"><span class="choice-icon"><img src="{{ theme:image_url file="choice-1.png" }}" /></span>Please Choose your <?php echo $val; ?></div>
	                            		<input type="hidden" value="<?php echo $key; ?>" name="id_choice[]" />
	                            		
	                            		<ul class="sub-list">
	                            		<?php 
	                            			
	                            			foreach ($choice as $key1 => $val1) {
	                            				if($key == $key1) {
	                            					
													foreach ($val1 as $key2 => $kid1) {
										
										?>
														 <li>
					                                        <div class="choice-title">
					                                        	<input type="radio" class="radio" name="list_choice[<?php echo $key1;?>]" value="<?php echo $key2; ?>"/><label></label><span><?php echo $kid1; ?></span>
					                                       	</div>
					                                        <!--<div class="choice-description">Description about medium rare.</div>-->
					                                    </li>				
										<?php				
													}
	                            				}
											}
	                            		?>
	                            		
	                            		</ul> 
	                            	</li>		        		
	        					<?php } ?>	
	        					</ul>							
							<?php }	?>
		              		<div class="button-wrapper">
		                    	<a href="<?php echo site_url().'/product/front_end/get_category'; ?>" class="button-auto cream">Cancel</a> 
		                    	<input type="hidden" name="id_item" value="<?php echo $value->id_item; ?>"  />
		                    	<input type="hidden" name="price_per_item" value="<?php echo $value->price_item; ?>"  />
		                    	<script type="text/javascript">
		                    		$(document).ready(function(){
		                    		$("#qty").val(localStorage.getItem('temp_qty'));
		                    		});
		                    	</script>
		                    	<input type="hidden" name="qty" id="qty" value="" />
		                    	<button type="submit" name="submit" id="submit" class="button-auto orange">Add to Cart</button>
		                  </div>
		            	<?php } ?>
	            	</form>
              	</div>
             	<div class="clear"></div>
			</div>
		<!--	
		<div class="menu-mobile-title">Our Services</div>
		{{ widgets:instance id="1"}}
		-->
		<div class="clear"></div>
	</div>
</div>
			