<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<section class="title">
    <h4><?php echo $title;?></h4>
</section>

<section class="item">
	<div class="content">
		<div class="form_inputs">
			<?php //echo var_dump($category); die();?>
			<form name="categories_form" enctype="multipart/form-data" method="post" action="<?php echo $url; ?>">
				<ul>
					<li>
						<label>Product Item <span>*</span></label><br />
						<input type="text" name="name_item" size="80" value="<?php echo set_value('name_item', isset($name_item) ? $name_item : ''); ?>" />
						<br />
					</li>
					<li>
                    	<label>Category <span>*</span></label><br /> 
                        <select name="id_category">						 	
                        	<?php 
                        	foreach($options_category as $ky=>$tess){
                        		$selek = '';
								if($id_category==$ky){
									$selek = 'selected="selected"';	
								}
                        		echo	
                        		'<option value="'.$ky.'" '.$selek.'>'.$tess.'</option>';
                        	}
                        	?>                   
                        </select>
                        <br />
                    </li>
                    <li>
						<label>Price <span>*</span></label><br />
						<input type="text" name="price_item" size="32" value="<?php echo set_value('price_item', isset($price_item) ? $price_item : ''); ?>" />
						<br />
					</li>
                     <li>
						<label>Status Item <span>*</span></label><br />
						 <select name="status">
							<option value="0" <?php echo $status == 0 ? 'selected="selected"':''; ?>>Draf</option>
							<option value="1" <?php echo $status == 1 ? 'selected="selected"':''; ?>>Available</option>
							<option value="1" <?php echo $status == 3 ? 'selected="selected"':''; ?>>Sold</option>
						</select>
                        <br />
					</li>
                    <li>
						<label>Description </label><br />
						<?php
						echo form_textarea(array('id' => 'description', 'name' => 'description', 'rows' => 30, 'value' => isset($description) ? $description : '','class' => 'wysiwyg-advanced'));
                        ?>
						<br />
					</li> 
					<li>
                    	<label>Thumbneil</label><br />
                    	<?php if($pic == '') { ?>
									
							<br />
							<img src="<?php echo base_url(); ?>uploads/thumbnail-widget.png">
							<br />
									
						<?php }	else { ?> 
									
							<br />
							<img src="<?php echo base_url(); ?>uploads/product/thumb/<?php echo $pic; ?>">
							<input type="hidden" name="pic" value="<?php echo $pic; ?>"
							<br /><br />
									
						<?php } ?>
						<input type="file" name="userfile" value="" />
						*) max size images 500x500<br />
                    </li>
				</ul>
				<div class="buttons">
					<button type="submit" name="submit" id="submit" class="btn blue">save</button>
			  		<button type="reset" class="btn blue">cencel</button>
				</div>
			</div>
		</div>
	</div>
</section>