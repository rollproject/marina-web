<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
?>

<section class="title">
	<h4><?php echo $judul; ?></h4> <a href="<?php echo $url; ?>" class="btn blue">Add Product Item</a> 
</section>
<section class="item">
	<div class="content">
	<table>
		<thead>
			<td>No</td>
			<td>#</td>
			<td>Product Item</td>
            <td>Price Item</td>
            <td width="450px;">Description</td>
			<td>Category</td>
            <td>Status</td>
			<td align="center">Menu</td>
		</thead>
		<?php if (isset($item)) : ?>
		<tbody>
			<?php 
				
				$no 	= 1;
				$uri	= $this->uri->segment(4);
				
				if($uri != '') {
					
					$no = (($uri - 1) * 10) + 1;
				}
				
				foreach ($item as $val) { 
			
			?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td>
						<?php if($val->pic == '') { ?>
							<img src="<?php echo base_url(); ?>uploads/thumbs.jpg" width="70" height="70">
						<?php } else { ?>
							<img src="<?php echo base_url(); ?>uploads/product/thumb/<?php echo $val->pic; ?>" width="70" height="70">
						<?php } ?>	
					</td>
					<td><?php echo $val->name_item; ?></td>
                    <td width="100px">Rp. <?php echo $val->price_item; ?>, 00</td>
                    <td width="400px"><?php echo $val->description; ?></td>
					<td width="100px"><?php echo $val->name_category; ?></td>
                    <td>
                    	<?php if($val->status == 0) {
                    		
							echo 'Darf';
                    	} else if($val->status == 1) {
                    		
							echo 'Available';
                    	} else {
                    		
							echo 'Sold';
                    	}?>
                    </td>
					<td width="150px">	
						<a href="<?php echo site_url().'/product/admin_item/update/'.$val->id_item; ?>" class="btn blue">Edit</a>
						<a href="<?php echo site_url().'/product/admin_item/delete/'.$val->id_item; ?>" class="btn blue">Delete</a>
					</td>
				</tr>
			<?php } ?>	
		</tbody>
		<?php endif; ?>
	</table>
	<?php echo $pagination['links']; ?>
	</div>
</section>

