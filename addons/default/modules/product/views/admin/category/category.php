<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
?>

<section class="title">
	<h4>Product Category</h4> 
	<a href="<?php echo site_url().'/product/admin_category/add'; ?>" class="btn blue">Add Category</a> &nbsp;
	<a href="<?php echo site_url().'/product/admin_category/export_process'; ?>" class="btn blue">Export Data</a> &nbsp;
	<a href="#" class="btn blue">Import Data</a> 
</section>
<section class="item">
	<div class="content">
	<table>
		<thead>
			<td>No</td>
			<td align="center">#</td>
			<td>Name Category</td>			
            <td width="560px">Description</td>
			<td>Status</td>
			<td align="center">Menu</td>
		</thead>
		<?php if (isset($category)) : ?>
		<tbody>
			<?php 
				
				$no = 1;
				foreach ($category as $val) { 
			
			?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td>
						<?php if($val->pic == '') { ?>
							<img src="<?php echo base_url(); ?>uploads/thumbs.jpg" width="70" height="70">
						<?php } else { ?>
							<img src="<?php echo base_url(); ?>uploads/category/thumb/<?php echo $val->pic; ?>" width="70" height="70">
						<?php } ?>						
					</td>
					<td><?php echo $val->name_category; ?></td>
                    <td><?php echo $val->description; ?></td>
                    <td>
					<?php
						if($val->status == 0) {
							
							echo 'Draf';
						}else {
							
							echo 'Publish';
						}
					?>
					</td>
					<td>	
						<a href="<?php echo site_url().'/product/admin_category/update/'.$val->id_category; ?>" class="btn blue">Edit</a>
						<a href="<?php echo site_url().'/product/admin_category/delete/'.$val->id_category; ?>" class="btn red">Delete</a>
					</td>
				</tr>
			<?php } ?>	
		</tbody>
		<?php endif; ?>
	</table>
	<?php echo $pagination['links']; ?>
	</div>
</section>

