<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<section class="title">
    <h4><?php echo $title;?></h4>
</section>

<section class="item">
	<div class="content">
		<div class="form_inputs">
			
			<?php echo form_open_multipart($url); ?>
				<ul>
					<li>
						<label>Kategory <span>*</span></label><br />
						<input type="text" name="kategori" size="30" value="<?php echo set_value('kategori', isset($name_category) ? $name_category : ''); ?>" />
						<br />
					</li>
                    <li>
						<label>Description </label><br />
                        <?php
						echo form_textarea(array('id' => 'description', 'name' => 'description', 'rows' => 20, 'value' => isset($description) ? $description : '','class' => 'wysiwyg-advanced'));
                        ?>
						<br />
					</li>
                    <li>
                    	<label>Parent Category</label><br />
                        <select name="id_parent">						 	
                        	<?php 
                        	foreach($options_category as $ky=>$tess){
                        		$selek = '';
								if($id_parent==$ky){
									$selek = 'selected="selected"';	
								}
                        		echo	
                        		'<option value="'.$ky.'" '.$selek.'>'.$tess.'</option>';
                        	}
                        	?>                   
                        </select>
                   		<br />
                    </li>
                    <li>
                    	<label>Thumbneil</label><br />
                    	<?php if($pic == '') { ?>
									
							<br />
							<img src="<?php echo base_url(); ?>uploads/thumbnail-widget.png">
							<br />
									
						<?php }	else { ?> 
									
							<br />
							<img src="<?php echo base_url(); ?>uploads/category/thumb/<?php echo $pic; ?>">
							<input type="hidden" name="pic" value="<?php echo $pic; ?>"
							<br /><br />
									
						<?php } ?>
						<input type="file" name="userfile" value="" />
						*) max size images 500x500<br />
                    </li>
                    <li>
                    	<label>Status <span>*</span></label><br />
                    	<select name="status">
							<option value="0" <?php echo $status == 0 ? 'selected="selected"':''; ?>>Draf</option>
							<option value="1" <?php echo $status == 1 ? 'selected="selected"':''; ?>>Publish</option>
						</select><br />
                    </li>
				</ul>
				<div class="buttons">
					<button type="submit" name="submit" id="submit" class="btn blue">save</button>
			  		<a href="<?php echo site_url().'/product/admin_category'; ?>" class="btn blue">back</a>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</section>