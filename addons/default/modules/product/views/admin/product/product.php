<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
?>

<section class="title">
	<h4><?php echo $judul; ?></h4> 
</section>
<section class="item">
	<div class="content">
	<table>
		<thead>
			<td>No</td>
			<td>Name</td>
			<td>Email</td>
			<td>Address</td>
			<td>Date Order</td>
			<td>Total</td>
			<td>Status Order</td>
			<td>Payment</td>
            <td align="center">Menu</td>
		</thead>
		<?php if (isset($order)) : ?>
		<tbody>
			<?php 
				
				$no = 1;
				foreach ($order as $val) { 
			
			?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $val->name_customer; ?></td>
					<td><?php echo $val->email_customer; ?></td>
					<td><?php echo $val->address_customer; ?></td>
					<td><?php echo $val->date_order; ?></td>
                    <td>Rp. <?php echo $val->total; ?></td>
					<td><?php echo $val->status_order; ?></td>
					<td><?php echo $val->payment_method; ?></td>
                    <td>	
						<a href="<?php echo site_url().'/product/admin/detail/'.$val->id_order; ?>" class="btn blue">Detail</a>
					</td>
				</tr>
			<?php } ?>	
		</tbody>
		<?php endif; ?>
	</table>
	</div>
</section>

