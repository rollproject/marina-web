<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<section class="title">
    <h4><?php echo $judul; ?></h4>
</section>

<section class="item">
	<div class="content">
		<div class="form_inputs">
			<?php //echo var_dump($category); die();?>
			<form name="detail_order" method="post" action="#">
				<h2>Customer Information</h2>
				<table>
					<thead>
						<td>Name Customer</td>
						<td>Email Customer</td>
						<td>No. Phone</td>
						<td>Address</td>						
					</thead>
					<tbody>
						<tr>
							<td><?php echo $order->name_customer; ?></td>
							<td><?php echo $order->email_customer; ?></td>
							<td><?php echo $order->phone_customer; ?></td>
							<td><?php echo $order->address_customer; ?></td>	
						</tr>
					</tbody>
					
				</table>
				<h2>Customer Order</h2>
				<table>
					<thead>
						<td>No. Order</td>
						<td>Date Order</td>
						<td>Payment</td>
						<td>Service</td>						
					</thead>
					<tbody>
						<tr>
							<td><?php echo $order->id_order; ?></td>
							<td><?php echo $order->date_order; ?></td>
							<td><?php echo $order->payment_method; ?></td>
							<td><?php echo $order->service_method; ?></td>	
						</tr>
					</tbody>
					
				</table>
				<h2>Order</h2>
				<table>
					<thead>
						<td>No</td>
						<td>Name Item</td>
						<td>Choice</td>
						<td>Quantity</td>
						<td>Price</td>
						<td>Total</td>						
					</thead>
					<?php if (isset($list_order)) : ?>
					<tbody>
						<?php 
							
							$no 		= 1;
							$total 		= 0;
							$subtotal 	= 0; 
							$tax		= 0;
							foreach ($list_order as $val) {
								 
								 $total 	= $val->qty * $val->price_per_item;
								 $subtotal	+= $total;
								 $tax		= (10/100) * $subtotal;
								 $all		= $subtotal + $tax;
						
						?>
							<tr>
								<td><?php echo $no++; ?></td>
								<td><?php echo $val->name_item; ?></td>
								<td><?php echo $val->choice_method; ?></td>
								<td><?php echo $val->qty; ?></td>
								<td><?php echo $val->price_per_item; ?></td>
								<td>Rp. <?php echo $total; ?></td>					
							</tr>
						<?php } ?>	
							<tr>
								<td colspan ="4" ></td>
								<td style="font-size: 16px;"><b>Total </b></td>
								<td style="font-size: 16px;"><b>Rp. <?php echo $subtotal; ?>, 00</b></td>								
							</tr>
							<tr>
								<td colspan ="4" ></td>
								<td style="font-size: 16px;"><b>Tax (10%)  </b></td>
								<td style="font-size: 16px;"><b>Rp. <?php echo $tax; ?>, 00</b></td>								
							</tr>
							<tr>
								<td colspan ="4" ></td>
								<td><h2>Sub Total </h2></td>
								<td><h2>Rp. <?php echo $all; ?>, 00</h2></td>								
							</tr>
					</tbody>
					<?php endif; ?>
				</table>
			</form>
			<form action="#" method="post">	
				<select name="status">
					<option value="New Order">New Order</option>
					<option value="Cencel">Cencel</option>
					<option value="Pending">Pending</option>
					<option value="Send">Send</option>
				</select>
				<div class="buttons">
					<input type="hidden" name="id_order" value="<?php echo $order->id_order; ?>" />
					<button type="submit" name="submit" id="submit" class="btn blue">save</button>
			  		<button type="reset" class="btn blue">Delete</button>
				</div>
			</form>
		</div>
	</div>
</section>