<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<section class="title">
    <h4><?php echo $title;?></h4>
</section>

<section class="item">
	<div class="content">
		<div class="form_inputs">
			<?php //echo var_dump($category); die();?>
			<form name="choice_form" method="post" action="<?php echo $url; ?>">
				<ul>
					<li>
						<label>Choice Item <span>*</span></label><br />
						<input type="text" name="name_choice" size="80" value="<?php echo set_value('name_choice', isset($name_choice) ? $name_choice : ''); ?>" />
					</li>
				</ul>
				<div class="buttons">
					<button type="submit" name="submit" id="submit" class="btn blue">save</button>
			  		<button type="reset" class="btn blue">cencel</button>
				</div>
			</div>
		</div>
	</div>
</section>