<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
?>

<section class="title">
	<h4><?php echo $judul; ?></h4> <a href="<?php echo $url; ?>" class="btn blue">Add Choice</a> 
</section>
<section class="item">
	<div class="content">
	<table>
		<thead>
			<td>No</td>
			<td>List Choice Item</td>
            <td>Choice Item</td>
			<td align="center">Menu</td>
		</thead>
		<?php if (isset($choice_list)) : ?>
		<tbody>
			<?php 
				
				$no = 1;
				foreach ($choice_list as $val) { 
			
			?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $val->name_list_choice; ?></td>
                    <td><?php echo $val->name_choice?></td>
                    <td>	
						<a href="<?php echo site_url().'/product/admin_choice/update_list/'.$val->id_list; ?>" class="btn blue">Edit</a>
						<a href="<?php echo site_url().'/product/admin_choice/delete_list/'.$val->id_list; ?>" class="btn blue">Delete</a>
					</td>
				</tr>
			<?php } ?>	
		</tbody>
		<?php endif; ?>
	</table>
	</div>
</section>

