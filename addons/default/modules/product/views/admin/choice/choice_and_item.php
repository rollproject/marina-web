<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
?>

<section class="title">
	<h4><?php echo $judul; ?></h4> <a href="<?php echo $url; ?>" class="btn blue">Add Item & Choice</a> 
</section>
<section class="item">
	<div class="content">
	<table>
		<thead>
			<td>No</td>
			<td>Name Item</td>
			<td>Choice Item</td>
			<td align="center">Menu</td>
		</thead>
		<?php if (isset($item_choice)) : ?>
		<tbody>
			<?php 
				
				$no = 1;
				foreach ($item_choice as $val) { 
			
			?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $val->name_item; ?></td>
					<td><?php echo $val->name_choice; ?></td>
                    <td>	
						<a href="<?php echo site_url().'/product/admin_choice/update_item_choice/'.$val->id_master; ?>" class="btn blue">Edit</a>
						<a href="<?php echo site_url().'/product/admin_choice/delete_master/'.$val->id_master; ?>" class="btn blue">Delete</a>
					</td>
				</tr>
			<?php } ?>	
		</tbody>
		<?php endif; ?>
	</table>
	</div>
</section>

