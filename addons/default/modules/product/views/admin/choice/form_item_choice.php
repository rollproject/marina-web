<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<section class="title">
    <h4><?php echo $title;?></h4>
</section>

<section class="item">
	<div class="content">
		<div class="form_inputs">
			<?php //echo var_dump($category); die();?>
			<form name="choice_form" method="post" action="<?php echo $url; ?>">
				<ul>
                    <li>
                    	<label>Choice <span>*</span></label><br />
                        <select name="id_choice">
                        	<option value="0">-- select choice --</option>
                       		<?php foreach ($choice as $val) { ?>
                        	<option value="<?php echo $val->id_choice;?>"><?php echo $val->name_choice;?></option>	
                        	<?php } ?>
                        </select>
                    </li>
                    <li>
                    	<label>Item <span>*</span></label><br />
                        <select name="id_item">
                        	<option value="0">-- select choice --</option>
                       		<?php foreach ($item as $val1) { ?>
                        	<option value="<?php echo $val1->id_item;?>"><?php echo $val1->name_item;?></option>	
                        	<?php } ?>
                        </select>
                    </li>
				</ul>
				<div class="buttons">
					<button type="submit" name="submit" id="submit" class="btn blue">save</button>
			  		<button type="reset" class="btn blue">cencel</button>
				</div>
			</div>
		</div>
	</div>
</section>