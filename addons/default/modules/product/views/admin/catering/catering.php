<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
?>

<section class="title">
	<h4><?php echo $judul; ?></h4> <a href="<?php echo $url; ?>" class="btn blue">Add Catering</a> 
</section>
<section class="item">
	<div class="content">
	<table>
		<thead>
			<td>No</td>
			<td>Name</td>
			<td>Price</td>
            <td align="center">Menu</td>
		</thead>
		<?php if (isset($item)) : ?>
		<tbody>
			<?php 
				
				$no = 1;
				foreach ($item as $val) { 
			
			?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $val->name_catering; ?></td>
                    <td>Rp. <?php echo $val->price; ?></td>
                    <td>	
						<a href="<?php echo site_url().'/product/admin_catering/update/'.$val->id_catering; ?>" class="btn blue">Edit</a>
						<a href="<?php echo site_url().'/product/admin_catering/delete/'.$val->id_catering; ?>" class="btn blue">Delete</a>
					</td>
				</tr>
			<?php } ?>	
		</tbody>
		<?php endif; ?>
	</table>
	</div>
</section>

