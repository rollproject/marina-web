<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<section class="title">
    <h4><?php echo $title;?></h4>
</section>

<section class="item">
	<div class="content">
		<div class="form_inputs">
			<?php //echo var_dump($category); die();?>
			<form name="<?php echo $form_name; ?>" method="post" action="<?php echo $url; ?>">
				<ul>
					<li>
						<label>Name <span>*</span></label><br />
						<input type="text" name="name_catering" size="80" value="<?php echo set_value('name_catering', isset($name_catering) ? $name_catering : ''); ?>" />
					</li>
					<li>
						<label>Price <span>*</span></label><br />
						<input type="text" name="price" size="32" value="<?php echo set_value('price', isset($price) ? $price : ''); ?>" />
					</li>
                    <li>
						<label>Description <span>*</span></label><br />
                        <?php 
                        echo form_textarea(array('id' => 'desc', 'name' => 'desc', 'rows' => 30, 'value' => isset($desc) ? $desc : '','class' => 'wysiwyg-advanced')); ?>
					</li>
                   
				</ul>
				<div class="buttons">
					<button type="submit" name="submit" id="submit" class="btn blue">save</button>
			  		<button type="reset" class="btn blue">cencel</button>
				</div>
			</div>
		</div>
	</div>
</section>