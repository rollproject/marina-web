<div id="middle">
	<div class="container">
		<div class="menu-content">
			<div class="page-title">{{ pages:display slug="menu" }} {{custom_fields}} {{page_title}} {{/custom_fields}} {{ /pages:display }} </div>
			<div id="page-banner-wrap">
				{{ pages:display slug="menu" }} {{custom_fields}} {{ image_banner:img }} {{/custom_fields}} {{ /pages:display }}
				<span class="color_1"></span>
				<span class="color_2"></span>
				<span class="color_3"></span>
				<span class="color_4"></span>				
				<div class="clear"></div>
			</div> <!-- #page-banner-wrap -->
			<!--			
			<div class="page-description">{{ pages:display slug="menu" }} {{custom_fields}} {{body}} {{/custom_fields}} {{ /pages:display }} </div>
			-->
			<div class="general-wp">
				<?php if($this->session->flashdata('error') != '') echo   '<div class="error-message">'.$this->session->flashdata('error').'</div><br />';?>
				<?php if(count($category) > 0) { ?> 
					<ul class="menu-list">
					<?php 
						$count = 0;
						foreach ($category as $value) {
					?>
						<li>
							<div class="highlight-menu">
			                  	<div class="left-side">
			                       
			                        	<?php if($value->pic != '') { ?> 
			                        		 <a href="<?php echo site_url().'/product/front_end/get_category_by_id/'.$value->id_category; ?>">
				                        		<img class="thumbs" src="<?php echo base_url();?>uploads/category/thumb/<?php echo $value->pic; ?>">
				                        	 </a>
			                        	<?php } else { ?> 
			                        		<a href="<?php echo site_url().'/product/front_end/get_category_by_id/'.$value->id_category; ?>">
			                        			<img class="thumbs" src="{{ theme:image_url file="thumbs.jpg" }}" />
				                        		
				                        	</a>
			                        	<?php } ?>
			                        
			                     </div>
			                     <div class="right-side">
			                     	<h3><a href="<?php echo site_url().'/product/front_end/get_category_by_id/'.$value->id_category; ?>"><?php echo $value->name_category; ?></a></h3>
			                        <span class="menu-description"><?php echo $value->description; ?></span>
			                     </div>
			                     <div class="clear"></div>
			 				</div>
			               	<div class="button-menu">
			               		<div class="left-side"><span class="view-menu">
			               			<a href="<?php echo site_url().'/product/front_end/get_category_by_id/'.$value->id_category; ?>" class="button-auto orange">View Menu</a>
			               		</span></div>
			                    <div class="clear"></div>
			                </div>
			                <div class="clear"></div>
						</li>		
						<?php } ?>
					</ul>
					<?php } else { ?>
						
					<?php } ?>
				<div class="clear"></div>
			</div>
		<!--	
		<div class="menu-mobile-title">Our Services</div>
		{{ widgets:instance id="1"}}
		-->
		<div class="clear"></div>
	</div>
</div>
			