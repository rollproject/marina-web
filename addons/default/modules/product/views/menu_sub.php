<script type="text/javascript">

	function insertTemp(index) {

		localStorage.setItem("temp_qty",$("#qty"+index).val());
	}
	
	check_number = function(evt){   
    	
    	var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode( key );
        var regex = /[0-9]|\./;
       
        if( !regex.test(key) ) {
          
          theEvent.returnValue = false;
          if(theEvent.preventDefault) theEvent.preventDefault();
        
        }
    }
</script>
<div id="middle">
	<div class="container">
		<div class="menu-content">
			<div class="page-title">{{ pages:display slug="menu" }} {{custom_fields}} {{page_title}} {{/custom_fields}} {{ /pages:display }} </div>
			<div id="page-banner-wrap">
				{{ pages:display slug="menu" }} {{custom_fields}} {{ image_banner:img }} {{/custom_fields}} {{ /pages:display }}
				<span class="color_1"></span>
				<span class="color_2"></span>
				<span class="color_3"></span>
				<span class="color_4"></span>				
				<div class="clear"></div>
			</div> <!-- #page-banner-wrap -->
			<!--			
			<div class="page-description">
				{{ pages:display slug="menu" }} {{custom_fields}} {{body}} {{/custom_fields}} {{ /pages:display }} 
				<br /><br />
                <center><a href="" class="button-auto orange">Read our terms & conditions</a></center> 
			</div>
			-->
			<div class="general-wp">
				<p class="side-padding">
                	<span class="tOrange sub-title-2"><?php echo $judul; ?></span><br />
                    <span class="tDark"><i><?php echo $desc; ?></i></span>
                </p>
                <?php if($this->session->flashdata('error') != '') echo   '<div class="error-msg">'.$this->session->flashdata('error').'</div>';?>
                 <div class="cart-box">
                	<div class="left-side">
                    	<span class="side-wrapper"><a href="<?php echo site_url().'/product/front_end/get_category'; ?>">Kembali</a></span>
                    	<div class="clear"></div>
                   	</div>
                    <div class="right-side">
                    	<span class="side-wrapper"><a href="<?php echo site_url().'/product/front_end/view_cart'; ?>" class="view">View Cart</a></span>
                      	<?php if($jml > 0) { ?>
                    		
							<span class="side-wrapper">Food's Cart : <span class="total-cart"><?php echo $jml; ?></span> item's</span>
							
                    	<?php } else { ?> 
                    		
                    		<span class="side-wrapper">Food's Cart : <span class="total-cart">0</span> item's</span>
                    	<?php } ?>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
              	</div>
                <div class="cart-box-mobile">
                	<div class="dark-line"></div>
                    <div class="left-side">
                    	<?php if($jml > 0) { ?>
                    		
							<span class="side-wrapper">Food's Cart : <span class="total-cart"><?php echo $jml; ?></span> item's</span>
							
                    	<?php } else { ?> 
                    		
                    		<span class="side-wrapper">Food's Cart : <span class="total-cart">0</span> item's</span>
                    	<?php } ?>   
                        <div class="clear"></div>
                   	</div>
                    <div class="right-side">
                    	<span class="side-wrapper"><a href="<?php echo site_url().'/product/front_end/view_cart'; ?>" class="view">View Cart</a></span>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
              	</div>
               	<div class="clear"></div>
				<?php if(count($category) > 0) { ?> 
					<ul class="menu-list">
					<?php 
						$count = 0;
						foreach ($category as $value) {
					?>
						<li>
							<div class="highlight-menu">
			                  	<div class="left-side">
			                        <?php if($value->pic != '') { ?> 
			                        		 <a href="<?php echo site_url().'/product/front_end/get_category_by_id/'.$value->id_category; ?>">
				                        		<img class="thumbs" src="<?php echo base_url();?>uploads/category/thumb/<?php echo $value->pic; ?>">
				                        	 </a>
			                        	<?php } else { ?> 
			                        		<a href="<?php echo site_url().'/product/front_end/get_category_by_id/'.$value->id_category; ?>">
			                        			<img class="thumbs" src="{{ theme:image_url file="thumbs.jpg" }}" />
				                        		
				                        	</a>
			                        	<?php } ?>
			                     </div>
			                     <div class="right-side">
			                     	<h3><a href="<?php echo site_url().'/product/front_end/get_category_by_id/'.$value->id_category; ?>"><?php echo $value->name_category; ?></a></h3>
			                        <span class="menu-description"><?php echo $value->description; ?></span>
			                     </div>
			                     <div class="clear"></div>
			 				</div>
			               	<div class="button-menu">
			               		<div class="left-side"><span class="view-menu">
			               			<a href="<?php echo site_url().'/product/front_end/get_category_by_id/'.$value->id_category; ?>" class="button-auto orange">View Menu</a>
			               		</span></div>
			                    <div class="clear"></div>
			                </div>
			                <div class="clear"></div>
						</li>		
						<?php } ?>
					</ul>
					<?php } else { } ?>
					<div class="clear"></div>
					<?php if(count($item) > 0) { ?> 
					<ul class="menu-list">
					<?php 
						$count = 0;
						foreach ($item as $items) {
							
					?>
						<li>
							<div class="highlight-menu">
	                        	<div class="left-side">
	                        		<?php if($items->pic != '') { ?> 
			                       		 <a class="fancybox" href="<?php echo base_url();?>uploads/product/<?php echo $items->pic; ?>" title="<?php echo $items->name_item; ?>">
				                       		<img class="thumbs" src="<?php echo base_url();?>uploads/product/thumb/<?php echo $items->pic; ?>">
				                       	 </a>
			                       	<?php } else { ?> 
			                       		<a class="fancybox" href="{{ theme:image_url file="thumbs.jpg" }}" title="<?php echo $items->name_item; ?>">
			                       			<img class="thumbs" src="{{ theme:image_url file="thumbs.jpg" }}" />				                        		
				                       	</a>
			                       	<?php } ?>
			   
	                        	</div>
	                            <div class="right-side">
	                            	<h3><?php echo $items->name_item; ?></h3>
	                                <span class="menu-description"><?php echo $items->description; ?></span>
	                            </div>
	                            <div class="clear"></div>
	                       	</div>
	                       
	                        <div class="button-menu">
	                        	<div class="left-side"> <span class="price-menu">IDR <?php echo $items->price_item; ?>,00</span></div>
	                            <div class="right-side">
	                            	<span class="quantity">
	                                	<label>Quantity :</label>
	                                    <input type="number" name="qty" id="qty<?php echo $count;?>" onchange="insertTemp('<?php echo $count;?>')" value=""/>
	                               	</span>
	                                <span class="select"><a href="<?php echo site_url().'/product/front_end/detail_item/'.$items->id_item; ?>" class="button-auto orange">Select</a></span>
	                                <div class="clear"></div>
	                          	</div>
	                            <div class="clear"></div>
	                      	</div>
	                    	<div class="clear"></div>
                        </li>
						<?php 
						$count++;
						} ?>
					</ul>
					<?php } ?>
				<div class="clear"></div>
			</div>
		<!--	
		<div class="menu-mobile-title">Our Services</div>
		{{ widgets:instance id="1"}}
		-->
		<div class="clear"></div>
	</div>
</div>

			