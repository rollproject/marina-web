<div id="middle">
	<div class="container">
		<div class="home-welcome">
			<div class="welcome-text">{{ pages:display slug="home" }} {{custom_fields}} {{page_title}} {{/custom_fields}} {{ /pages:display }}</div>
			<div class="mobile-background">
				<img src="{{ theme:image_url file="bg1.jpg" }}" id="mobile-headline">
				<div class="welcome-text2">{{ pages:display slug="home" }} {{custom_fields}} {{page_title}} {{/custom_fields}} {{ /pages:display }}</div>
			</div>
			<div class="highlight">
				<div class="delecious-title">Find Delecious Menu in Single Click!</div>
				<div class="delecious-text">{{ pages:display slug="home" }} {{custom_fields}} {{body}} {{/custom_fields}} {{ /pages:display }}</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<!--
		<div class="slider">

			<ul id="images" class="rs-slider">
				<li class="group">
					<a href="#">
				    	<img src="{{ theme:image_url file="slider-sample.png" }}">
				    </a>
				    <div class="rs-caption info">
				    	<h3 class="title">Steak Sandwich</h3>
				        <p>Stop by our fresh, made to order, gourment sandwich. <a href="#" class="button">Learen More</a></p>
				    </div>
				</li>
				<li class="group">
					<a href="#">
				    	<img src="{{ theme:image_url file="slider-sample.png" }}">
				    </a>
				    <div class="rs-caption info">
				    	<h3 class="title">Steak Sandwich 1</h3>
				        <p>Stop by our fresh, made to order, gourment sandwich. <a href="#" class="button">Learen More</a></p>
				    </div>
				</li>
				<li class="group">
					<a href="#">
				    	<img src="{{ theme:image_url file="slider-sample.png" }}">
				    </a>
				    <div class="rs-caption info">
				    	<h3 class="title">Steak Sandwich 2</h3>
				        <p>Stop by our fresh, made to order, gourment sandwich. <a href="#" class="button">Learen More</a></p>
				    </div>
				</li>
			</ul>
			<div class="clear"></div>
		</div>
		-->
		<div class="clear"></div>
		<div class="download">
			<div class="left-side">
				<div class="android-logo">
					<img src="{{ theme:image_url file="android-logo.png" }}">
				</div>
				<span class="info">Free MarinaResto application<br/>GET IN ON Google Play</span>
				<div class="clear"></div>
			</div>
			<div class="right-side">
				<a href="#" class="download-btn">Download APP</a>
			</div>
		</div>
		<div class="clear"></div>
		<div class="list-foods">
		<?php if(count($category) > 0) { ?> 
			<ul>
				<?php 
					$count = 0;
					foreach ($category as $value) {
				?>
					<li>
						<div class="thumbs">
							<?php if($value->pic != '') { ?> 
			                   	<a href="<?php echo site_url().'/product/front_end/get_category_by_id/'.$value->id_category; ?>">
			                    	<img src="<?php echo base_url();?>uploads/category/<?php echo $value->pic; ?>">
			                    </a>
			               	<?php } else { ?> 
			                   	<a href="<?php echo site_url().'/product/front_end/get_category_by_id/'.$value->id_category; ?>">
			                       	<img src="{{ theme:image_url file="sample-home.png" }}" />
			                    </a>
					         <?php } ?>
						</div>
						<div class="title">
							<a href="<?php echo site_url().'/product/front_end/get_category_by_id/'.$value->id_category; ?>"><?php echo $value->name_category; ?></a>
						</div>
					</li>
				<?php } ?>
				<div class="clear"></div>
			</ul>
		<?php } else { ?>
						
		<?php } ?>
		</div>
		<div class="box-4">
			{{ widgets:instance id="3"}}
		</div>
	</div>
</div>