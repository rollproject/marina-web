<script type="text/javascript">
	change_qty = function(element){
        
        if ($(element).val() == '' || $(element).val() <= 9) {
            
            $(element).val('10')
        }
    }
</script>
<div id="middle">
	<div class="container">
		<div class="menu-content">
			<div class="page-title">{{ pages:display slug="catering" }} {{custom_fields}} {{page_title}} {{/custom_fields}} {{ /pages:display }}</div>
			<div class="page-description">
				{{ pages:display slug="catering" }} {{custom_fields}} {{body}} {{/custom_fields}} {{ /pages:display }}
			</div>
			<div class="catering4">
            	<ul>
                	<li><img src="{{ theme:image_url file="cat1.jpg" }}"></li>
                	<li><img src="{{ theme:image_url file="cat2.jpg" }}"></li>
                	<li><img src="{{ theme:image_url file="cat3.jpg" }}"></li>
                	<li><img src="{{ theme:image_url file="cat4.jpg" }}"></li>
                </ul>
                <div class="clear"></div>
 	        </div>
 	        
			<div class="catering-packages">
				<p class="text"><span class="tOrange">Fusce dapibus</span>, <b>tellus ac cursus commodo</b>, tortor mauris condimentum nibh, ut <i>fermentum</i> massa justo sit amet risus.
                Maecenas faucibus mollis interdum. Nulla vitae elit libero, <u>a pharetra augue</u>.</p>				
				<?php if($this->session->flashdata('error') != '') echo   '<div class="error-message">'.$this->session->flashdata('error').'</div><br />';?>
				<div class="cart-box">
					<div class="left-side">
						<span class="side-wrapper"><a href="<?php echo site_url().'/product/front_end/get_catering'; ?>">Kembali</a></span>    
                    	<div class="clear"></div>
                  	</div>
                    <div class="right-side">
                    	<span class="side-wrapper"><a href="<?php echo site_url().'/product/front_end/view_cart_catering'; ?>" class="view">View Cart</a></span>
                    	<?php if($jml > 0) { ?>
                    		
							<span class="side-wrapper">Food's Cart : <span class="total-cart"><?php echo $jml; ?></span> item's</span>
							
                    	<?php } else { ?> 
                    		
                    		<span class="side-wrapper">Food's Cart : <span class="total-cart">0</span> item's</span>
                    	<?php } ?>
                    	 <div class="clear"></div>
                   	</div>
                    <div class="clear"></div>
             	</div>
                <div class="cart-box-mobile">
                	<div class="dark-line"></div>
                    <div class="left-side">
                    	<?php if($jml > 0) { ?>
                    		
							<span class="side-wrapper">Food's Cart : <span class="total-cart"><?php echo $jml; ?></span> item's</span>
							
                    	<?php } else { ?> 
                    		
                    		<span class="side-wrapper">Food's Cart : <span class="total-cart">0</span> item's</span>
                    	<?php } ?>
                    	<div class="clear"></div>
                  	</div>
                    <div class="right-side">
                    	<span class="side-wrapper"><a href="<?php echo site_url().'/product/front_end/view_cart_catering'; ?>" class="view">View Cart</a></span>
                        <div class="clear"></div>
                   	</div>
                    <div class="clear"></div>
             	</div>
				<?php if(count($catering) > 0) { ?>
				<ul>
					<?php foreach ($catering as $value) { ?>
						<li>
							<div class="title-wp">
	                        	<span class="title"><?php echo $value->name_catering; ?></span>
	                            <span class="price">IDR <?php echo $value->price; ?> / PAX</span>
	                            <div class="clear"></div>
	                      	</div>
	                      	<?php echo $value->desc; ?>
	                      	<form action="<?php echo site_url().'/product/front_end/add_catering'; ?>" method="post">
	                      	<div class="quantity">
	                        	<span>Quantity</span>
	                            <input type="text" class="input-text" onchange="change_qty(this)" name="qty" id="qty" value="10" style="text-align: center;">
	                            <input type="hidden" value="<?php echo $value->id_catering ?>" name="id_catering" />
	                            <input type="hidden" value="<?php echo $value->price; ?>" name="price" />
	                            <button type="submit" name="submit" id="submit" class="button orange" style="height:40px;">Select</button>
	                       	</div>
	                       	</form>
						</li>
					<?php } ?>
				</ul>
				<?php }else { ?> 
							
				<?php } ?>
			</div>		
			<!--	
			<div class="menu-mobile-title">Our Services</div>
			{{ widgets:instance id="1"}}
			-->
			<div class="clear"></div>
		</div>
	</div>
</div>
			