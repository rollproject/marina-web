<div id="middle">
	<div class="container">
		<div class="menu-content">
			<div class="page-title">Order Form</div>
			<div id="page-banner-wrap">
				{{ pages:display slug="menu" }} {{custom_fields}} {{ image_banner:img }} {{/custom_fields}} {{ /pages:display }}
				<span class="color_1"></span>
				<span class="color_2"></span>
				<span class="color_3"></span>
				<span class="color_4"></span>				
				<div class="clear"></div>
			</div> <!-- #page-banner-wrap -->			
			<div class="page-description">
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.
   				ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                <br />  <br />
                <center><a class="button-auto orange" href="">Read our terms & conditions</a></center>
			</div>
			<div class="general-wp">
				<p class="side-padding">
                	<span class="tOrange sub-title-2"><?php echo $judul; ?></span><br />
                    <span class="tLight"><i>Please check your order list</i></span>
               	</p>
               	<?php if($this->session->flashdata('succes') != '') echo   '<div class="succes-message">'.$this->session->flashdata('succes').'</div><br />';?>
               	<?php if($this->session->flashdata('error') != '') echo   '<div class="error-message">'.$this->session->flashdata('error').'</div><br />';?>
				 <div class="cart-box">
                	<div class="left-side">
                    	<span class="side-wrapper"><a href="<?php echo site_url().'/product/front_end/get_category'; ?>">Kembali</a></span>
                    	<div class="clear"></div>
                   	</div>
                    <div class="right-side">
                    	<span class="side-wrapper"><a href="<?php echo site_url().'/product/front_end/view_cart'; ?>" class="view">View Cart</a></span>
                      	<?php if($jml > 0) { ?>
                    		
							<span class="side-wrapper">Food's Cart : <span class="total-cart"><?php echo $jml; ?></span> item's</span>
							
                    	<?php } else { ?> 
                    		
                    		<span class="side-wrapper">Food's Cart : <span class="total-cart">0</span> item's</span>
                    	<?php } ?>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
              	</div>
                <div class="cart-box-mobile">
                	<div class="dark-line"></div>
                    <div class="left-side">
                    	<?php if($jml > 0) { ?>
                    		
							<span class="side-wrapper">Food's Cart : <span class="total-cart"><?php echo $jml; ?></span> item's</span>
							
                    	<?php } else { ?> 
                    		
                    		<span class="side-wrapper">Food's Cart : <span class="total-cart">0</span> item's</span>
                    	<?php } ?>   
                        <div class="clear"></div>
                   	</div>
                    <div class="right-side">
                    	<span class="side-wrapper"><a href="<?php echo site_url().'/product/front_end/view_cart'; ?>" class="view">View Cart</a></span>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
              	</div>
				<ul class="order-list">
				<?php if(count($item) > 0) { ?>
					<?php 
						
						$count 		= 0;
						$total		= 0;
						$sub_total 	= 0;
						$total_qty	= 0;
						$tax		= 0;
						$all		= 0;
						
						foreach ($item as $k=>$value) {
						
							$total 		= $value->qty * $value->price_item;
							$sub_total	+= $total;
							$total_qty	+= $value->qty;
							$tax		= (10/100) * $sub_total; 
							$all		= $tax + $total;
					?> 
					<li>
						<form method="post" action="<?php echo site_url().'/product/front_end/update_order'; ?>">
                   		<div class="highlight-menu">                   			
                        	<div class="left-side"><img class="thumbs" src="{{ theme:image_url file="thumbs.jpg" }}" /></div>
                            <div class="right-side">
                            	<h3><?php echo $value->name_item; ?></h3>
                                <span class="price-menu">IDR <?php echo $value->price_item; ?>,00</span>
                                <span class="quantity">
                                	<label>Quantity :</label>
                                    <input type="text" name="qty" value="<?php echo $value->qty; ?>" style="height: 18px;"/>
                                </span>
                                <span class="sub-total-wp">
                                	Sub-total : <span class="sub-total">IDR <?php echo $total; ?>,00</span>
                    			</span>
                                <a href="<?php echo site_url().'/product/front_end/delete_order/'.$value->id_temp_order; ?>" class="cancel button dark">Cancel</a>
                                <input type="hidden" name="id_temp_order" value="<?php echo $value->id_temp_order; ?>" />
                            	&emsp;<button type="submit" name="submit" id="submit" class="revise button orange" style="padding: 0 0 12px; vertical-align: middle;">Revise Order</button>
                            </div>
                        	<div class="clear"></div>                        	
                     	</div>
                     	<?php if($value->choice != '') { ?>
             			<ul class="choice-list">
             				<?php 
             					foreach ($choice[$k] as $key_ => $choice_) { 
             					//var_dump($choice_['name']);
             					?>	 
             					                      	
	                        	<li>
	                        		<input type="checkbox" class="check" checked="checked"/><label></label>
	                                <span class="title">
	                                	<?php echo $choice_['name']; ?>
	                                	
	                                </span>
	                                <span class="description"><?php echo $choice_['kid']; ?></span>
	                            </li>
                            <?php } ?>
                        </ul>
                        <?php } else { ?> 
                        	
                        <?php } ?>
                        <div class="clear"></div>
                        <div class="button-menu">
                        	<div class="left-side">
                            	<span class="price-menu">IDR <?php echo $value->price_item; ?>,00</span>
                            </div>
                            <div class="right-side">
                            	<a href="#" class="cancel button dark">Cancel</a>
                                <a class="revise button orange">Revise Order</a>
                           	</div>
                        	<div class="clear"></div>
                    	</div>
                    	</form>
                    </li>
                	<?php } ?>
				</ul> 	
				<div class="clear"></div>
				<div class="total-order side-padding">
                	<div class="title"><img class="icon" src="{{ theme:image_url file="order-icon.png" }}" /> Total Order <!--- ID <span class="order-id">1042582</span>--></div>
                    <div class="note">Note : Please check your order!</div>
                    <div><label class="tOrange">Total Quantity</label><span class="total-quantity"><?php echo $total_qty; ?></span></div>
                    <div class="dark-line"></div>
                    <div><label>Total Price</label><span class="total-price">IDR <?php echo $sub_total; ?>,00</span></div>
                    <div class="dark-line"></div>
                    <div><label>Tax (10%)</label><span class="taxes">IDR <?php echo $tax; ?>,00</span></div>
                    <div class="dark-line"></div>
                     <div><label>Total </label><span class="taxes">IDR <?php echo $all; ?>,00</span></div>
                    <div class="dark-line"></div>
                    <!--<a class="button-auto dark cancel-order" >Cancel</a>-->
                    <a href="<?php echo site_url().'/product/front_end/finish_order'; ?>" class="button-auto orange continue-order">Finish Order</a>&emsp;
                    <a href="<?php echo site_url().'/product/front_end/get_category'; ?>" class="button-auto orange continue-order">Continue Order</a>
                 </div>
            	 <?php } else { ?> 
						
					sory, your cart empty.
				
				<?php } ?>
			</div>
		<!--	
		<div class="menu-mobile-title">Our Services</div>
		{{ widgets:instance id="1"}}
		-->
		<div class="clear"></div>
	</div>
</div>
			