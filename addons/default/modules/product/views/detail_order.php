<div id="middle">
	<div class="container">
		<div class="menu-content">
			<div class="page-title">Order Form</div>
			<div id="page-banner-wrap">
				{{ pages:display slug="menu" }} {{custom_fields}} {{ image_banner:img }} {{/custom_fields}} {{ /pages:display }}
				<span class="color_1"></span>
				<span class="color_2"></span>
				<span class="color_3"></span>
				<span class="color_4"></span>				
				<div class="clear"></div>
			</div> <!-- #page-banner-wrap -->			
			<div class="page-description">
				{{ pages:display slug="menu" }} {{custom_fields}} {{body}} {{/custom_fields}} {{ /pages:display }} 
				<br />  <br />
                <center><a class="button-auto orange" href="">Read our terms & conditions</a></center>
			</div>
			<div class="general-wp">
				<p class="side-padding">
                	<span class="tOrange sub-title-2"><?php echo $judul; ?></span><br />
               	</p>
				<ul class="order-list">
				<?php if(count($list_order) > 0) { ?>
					<?php 
						
						$count 		= 0;
						$total		= 0;
						$sub_total 	= 0;
						$total_qty	= 0;
						
						foreach ($list_order as $k=>$value) {
						
							$total 		= $value->qty * $value->price_per_item;
							$sub_total	+= $total;
							$total_qty	+= $value->qty; 
					?> 
					<li>
						<form method="post" action="<?php echo site_url().'/product/front_end/update_order'; ?>">
                   		<div class="highlight-menu">                   			
                        	<div class="left-side"><img class="thumbs" src="{{ theme:image_url file="thumbs.jpg" }}" /></div>
                            <div class="right-side">
                            	<h3><?php echo $value->name_item; ?></h3>
                                <span class="price-menu">IDR <?php echo $value->price_per_item; ?>,00</span>
                                <span class="quantity">
                                	<label>Quantity :</label>
                                    <input type="text" name="qty" readonly="readonly" value="<?php echo $value->qty; ?>" style="height: 18px;"/>
                                </span>
                                <span class="sub-total-wp">
                                	Sub-total : <span class="sub-total">IDR <?php echo $total; ?>,00</span>
                    			</span>
                            </div>
                        	<div class="clear"></div>                        	
                     	</div>
                     	<div class="clear"></div>
                        <div class="button-menu">
                        	<div class="left-side">
                            	<span class="price-menu">IDR <?php echo $value->price_per_item; ?>,00</span>
                            </div>
                        	<div class="clear"></div>
                    	</div>
                    	</form>
                    </li>
                	<?php } ?>
				</ul> 	
               	<div class="clear"></div>
				<div class="total-order side-padding">
                	<div class="title"><img class="icon" src="{{ theme:image_url file="order-icon.png" }}" /> Total Order <!--- ID <span class="order-id">1042582</span>--></div>
                    <div><label class="tOrange">Total Quantity</label><span class="total-quantity"><?php echo $total_qty; ?></span></div>
                    <div class="dark-line"></div>
                    <div><label>Total Price</label><span class="total-price">IDR <?php echo $sub_total; ?>,00</span></div>
                    <div class="dark-line"></div>
                 </div>
                 <div class="clear"></div>
				 <div class="total-order side-padding">
                	<div class="title">You'r Information Detail</div>
            		<div><label class="tOrange">Name</label><span class="total-quantity"><?php echo $order->name_customer; ?></span></div>
                    <div class="dark-line"></div>
                    <div><label class="tOrange">Address</label><span class="total-quantity"><?php echo $order->address_customer; ?></span></div>
                    <div class="dark-line"></div>
                    <div><label class="tOrange">Phone</label><span class="total-quantity"><?php echo $order->phone_customer; ?></span></div>
                    <div class="dark-line"></div>
                    <div><label class="tOrange">Payment</label><span class="total-quantity"><?php echo $order->payment_method; ?></span></div>
                    <div class="dark-line"></div>
                    <div><label class="tOrange">Service</label><span class="total-quantity"><?php echo $order->service_method; ?></span></div>
                    <div class="dark-line"></div>
                    <div><label class="tOrange">Message</label><span class="total-quantity"><?php echo $order->note; ?></span></div>
                    <div class="dark-line"></div>
                 </div>
                 <div class="left-side">
                 	<a class="revise button orange" href="<?php echo site_url().'/product/front_end/complate_order_finish'; ?>">Complate Order</a>
                 </div>
                 <div class="clear"></div>
            <?php } ?>
			</div>
		<!--	
		<div class="menu-mobile-title">Our Services</div>
		{{ widgets:instance id="1"}}
		-->
		<div class="clear"></div>
	</div>
</div>
			