<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
class Product_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
	}
	
	//global function
	function get_($order_by, $table) {
	
		$this->db->order_by($order_by);
		return $this->db->get($table)->result();
	}
	
	function delete_($table, $field_id, $id) {
		
		$this->db->delete($table, array($field_id => $id));
	}
	
	function insert_($table, $field) {
		
		return $this->db->insert($table, $field);
	}
	
	function update_($id_field, $id, $table, $field) {
		
		$this->db->where($id_field, $id);
		$this->db->update($table, $field);
	}
	
	/*function updateMoreParameter($id_field,$id_item,$table,$field){
		$this->db->where(array('id_session' => $id_field , 'id_item' => $id_item));
		$this->db->update($table, $field);
	}*/
	function updateMoreParameter($id_field,$table,$field){
		$this->db->where(array('id_temp_order' => $id_field));
		$this->db->update($table, $field);
	}
	
	function updateChoiceById_tempOrder($id_temp_order,$table,$field){
		$this->db->where('id_temp_order',$id_temp_order);
		$this->db->update($table, $field);
	}
	
	function update_qty_item($id_temp_order,$id_session,$table,$field){
		$this->db->where(array('id_temp_order' => $id_temp_order , 'id_session' => $id_session));
		$this->db->update($table, $field);
	}
	
	function update_Catering_tempOrder($id_session,$id_catering,$table,$field){
		$this->db->where(array('id_session' => $id_session,'id_catering'=>$id_catering));
		$this->db->update($table,$field);
	}
	
	function delete_Catering_tempOrder($id_session,$id_catering,$table,$field){
		$this->db->where(array('id_session' => $id_session,'id_catering'=>$id_catering));
		$this->db->delete($table,$field);
	}
	
	function get_all_($table, $field) {
					
		$this->db->select('*');
		$this->db->from($table);
		$this->db->order_by($field, 'ASC');
		return $this->db->get()->result();
	}
	
	function get_all_sort($table, $field, $val) {
					
		$this->db->select('*');
		$this->db->from($table);
		$this->db->order_by($field, $val);
		return $this->db->get()->result();
	}
	
	//edited
	function get_if_menu_exist($table,$id_session,$id_item){
		$this->db->select("*");
		$this->db->from($table);
		$this->db->where(array('id_session' => $id_session,'id_item'=>$id_item));
		return $this->db->get()->result();
	}
	
	function update_menu_if_exist($table,$id_temp_order,$field){
		//$this->db->where(array('id_session' => $id_field , 'id_item' => $id_item));
		$this->db->where('id_temp_order',$id_temp_order);
		return $this->db->update($table, $field);
	}
	
	function get_by_id_($table, $field, $id) {
			
		return $this->db->get_where($table, array($field => $id))->row();
	}
	
	function count_all_($table) {
		
		return $this->db->count_all($table);
	}
	
	//category
	function get_category() {
			
		$this->db->select('*');
		$result = $this->db->get_where('product_category', array('id_parent' => 0, 'status' => 1))->result();
		return $result;
			
	}
	
	//item
	function get_all_item() {
		
		$this->db->select('product_item.id_item, product_item.name_item, product_item.description, product_item.price_item, product_item.status, product_item.pic, product_item.slug, product_item.id_category, product_category.name_category');
		$this->db->from('product_item');
		$this->db->join('product_category', 'product_item.id_category = product_category.id_category', 'LEFT');
		$this->db->order_by('product_item.name_item', 'asc');
		return $this->db->get()->result();
	}
	
	//list choice
	function get_list_choice() {
		
		$this->db->select('*');
		$this->db->from('product_list_choice');
		$this->db->join('product_choice', 'product_list_choice.id_choice = product_choice.id_choice');
		$this->db->order_by('product_choice.name_choice', 'asc');
		return $this->db->get()->result();
	}
	
	function get_item_choice() {
		
		$this->db->select('*');
		$this->db->from('product_master_choice');
		$this->db->join('product_item', 'product_master_choice.id_item = product_item.id_item');
		$this->db->join('product_choice', 'product_master_choice.id_choice = product_choice.id_choice');
		$this->db->order_by('product_item.name_item', 'asc');
		return $this->db->get()->result();
	}

	//front end
	function get_all_category($table, $field, $val) {
		
		$this->db->select('*');
		$result = $this->db->get_where($table, array($field => $val, 'status' => 1))->result();
		return $result;
	
	}

	function get_item_by_category($id_category) {
		
		$this->db->select('product_item.id_item, product_item.name_item, product_item.description, product_item.price_item, product_item.status, product_item.pic, product_item.slug, product_item.id_category, product_category.name_category');
		$this->db->join('product_category', 'product_item.id_category = product_category.id_category');
		$result = $this->db->get_where('product_item', array('product_item.id_category' => $id_category))->result();
		return $result;
	}
	
	function get_item_cart($id) {
		
		$this->db->select('product_temp_order.id_temp_order, product_temp_order.choice, product_temp_order.id_item, product_temp_order.qty, product_item.name_item, product_item.price_item, product_item.pic');
		$this->db->join('product_item', 'product_temp_order.id_item = product_item.id_item');
		$result = $this->db->get_where('product_temp_order', array('product_temp_order.id_session' => $id))->result();	
		return $result;
	}
	
	function get_count_cart($id) {
		
		$this->db->select('SUM(qty) as total');
		$result = $this->db->get_where('product_temp_order', array('id_session' => $id))->result();
		return $result;
		
	}
	
	/*function get_item_by_id($id_item) {
		
		$this->db->select('product_item.id_item, product_item.name_item, product_item.price_item, product_item.pic, product_item.status, product_item.description, product_choice.id_choice, product_choice.name_choice, product_list_choice.name_list_choice');
		$this->db->join('product_master_choice', 'product_item.id_item = product_master_choice.id_item', 'LEFT');
		$this->db->join('product_choice', 'product_master_choice.id_choice = product_choice.id_choice', 'LEFT');
		$this->db->join('product_list_choice', 'product_choice.id_choice = product_list_choice.id_choice', 'LEFT');
		$result = $this->db->get_where('product_item', array('product_item.id_item' => $id_item))->result();
		return $result;
	}*/
	
	function get_item_by_id($id_item) {
		
		$this->db->select('*');
		$result = $this->db->get_where('product_item', array('product_item.id_item' => $id_item))->result();
		return $result;
	}
	
	function get_choice_by_item($id_item) {
		
		$this->db->select('*');
		$this->db->join('product_item', 'product_master_choice.id_item = product_item.id_item');
		$this->db->join('product_choice', 'product_master_choice.id_choice = product_choice.id_choice');
		$this->db->group_by('product_master_choice.id_choice');
		$result = $this->db->get_where('product_master_choice', array('product_master_choice.id_item' => $id_item))->result();
		return $result;
	}
	
	function get_kid_choice($id_choice) {
			
		$this->db->select('*');
		$this->db->join('product_list_choice', 'product_choice.id_choice = product_list_choice.id_choice');
		//$this->db->group_by('lc.id_choice');
		$result = $this->db->get_where('product_choice', array('product_choice.id_choice' => $id_choice))->result();
		return $result;
			
	}
	
	function cart($id) {
		
		$this->db->select('product_temp_order.id_temp_order, product_temp_order.id_session, product_temp_order.id_item, product_temp_order.price_per_item, product_temp_order.choice, product_temp_order.qty, product_item.name_item, product_item.price_item, product_item.pic');
		$this->db->join('product_item', 'product_temp_order.id_item = product_item.id_item');
		$result = $this->db->get_where('product_temp_order', array('product_temp_order.id_session' => $id))->result();
		return $result;
	}
	
	function insert_to_order($table, $field) {
		
		$this->db->insert($table, $field);
		return $this->db->insert_id();
	}
	
	function get_order($id) {
		
		$this->db->select('SUM(qty) as jumlah, SUM(price_per_item) as total');
		$result = $this->db->get_where('product_temp_order', array('id_session' => $id))->result();
		return $result;
	}
	
	function get_count_order($id_order) {
		
		$this->db->select('SUM(qty) as total');
		$result = $this->db->get_where('product_temp_order', array('id_session' => $id))->result();
		return $result;
		
	}

	function get_cart_choice($id_item) {
		
		$this->db->select('*');
		$result = $this->db->get_where('product_master_choice', array('id_item' => $id_item))->result();
		return $result;
	}

	function get_by_id_result($table, $field, $id) {
		
		$this->db->select('*');
		$this->db->join('product_item', 'product_list_order.id_item = product_item.id_item');
		$result = $this->db->get_where($table, array($field => $id))->result();	
		return $result;
	}
	
	function get_catering_cart($id) {
		
		$this->db->select('product_temp_catering.id_temp_order, product_temp_catering.id_catering, product_temp_catering.qty, product_catering.name_catering, product_catering.price');
		$this->db->join('product_catering', 'product_temp_catering.id_catering = product_catering.id_catering');
		$result = $this->db->get_where('product_temp_catering', array('product_temp_catering.id_session' => $id))->result();	
		return $result;
	}
	
	function get_count_cart_catering($id) {
		
		$this->db->select('SUM(qty) as jumlah, SUM(price) as total');
		$result = $this->db->get_where('product_temp_catering', array('id_session' => $id))->result();
		return $result;
		
	}
	
	function get_detail_order($id_order) {
		
		$this->db->select('*');
		$result = $this->db->get_where('product_order', array('id_order' => $id_order))->row();	
		return $result;
	}
	
	function get_list_order($id_order) {
		
		$this->db->select('*');
		$this->db->join('product_item', 'product_list_order.id_item = product_item.id_item');
		$result = $this->db->get_where('product_list_order', array('product_list_order.id_order' => $id_order))->result();	
		return $result;
	}
	
	function delete_food_tempOrder($id_session,$id_temp_order,$table,$field){
		$this->db->where(array('id_session'=>$id_session,'id_temp_order'=>$id_temp_order));
		$this->db->delete($table,$field);
	}
	
	function get_item_cart_mobile($id) {
		
		$this->db->select('product_temp_order.id_temp_order, product_temp_order.choice, product_temp_order.id_item, product_temp_order.qty, product_item.name_item, product_item.price_item, product_item.pic, product_item.id_category, product_category.name_category');
		$this->db->join('product_item', 'product_temp_order.id_item = product_item.id_item');
		$this->db->join('product_category', 'product_item.id_category = product_category.id_category');
		$this->db->order_by("product_temp_order.id_temp_order", "asc");
		$result = $this->db->get_where('product_temp_order', array('product_temp_order.id_session' => $id))->result();	
		return $result;
	}
}