<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
class Front_end extends Public_Controller {
	
	public function __construct() {
			
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model('testimonial_m');
		$this->load->helper('date');
		$this->template->set_layout('product.html');
		
		$this->load->library(array('recaptcha','form_validation'));
		$this->template->set('recaptcha_html',$this->recaptcha->recaptcha_get_html());
		
	}

	public function index() {
		
		$this->get_testimonial();
	}
	
	public function get_testimonial() {
		$title		= 'Marina Testimonial';
		$testi 		= $this->testimonial_m->get_testimonial();
		$url		= site_url().'/testimonial/front_end/add_process';
		
		$this->template
			 ->title($title)
			 ->set('testimonial', $testi)
			 ->set('url', $url)
			 ->build('testimonial');
	}
	
	
	
	function add_process() {
		$title			= "Marina Testimonial";
		$form_action	= site_url().'/testimonial/front_end/add_process'; 
		$status			= '';
		$pic 			= '';
		
		$nama_asli 					= $_FILES['userfile']['name'];	
		$judul 						= $this->input->post('name',TRUE);
		$config['file_name'] 		= $judul.'_'.'_'.$nama_asli;
		$config['upload_path'] 		= getcwd().'/uploads/testimonial/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size']			= '1024';
		$config['max_width']  		= '1600';
		$config['max_height']  		= '1200';
		
		$this->load->library('upload', $config);
		
		$this->form_validation->set_rules('name', 'Name', 'trim|requires');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('company', 'Company', '');
		$this->form_validation->set_rules('address', 'Address', '');
		$this->form_validation->set_rules('testimonial', 'Testimonial', 'trim|required');
		$this->form_validation->set_rules('pic', 'pic', '');
		$this->form_validation->set_rules('recaptcha_challenge_field', 'Security Code', 'trim|required|callback__recaptcha_check');
		
		
		if ($this->form_validation->run() == TRUE) {
			
			if($this->upload->do_upload()){
					
				$get_name = $this->upload->data();
				$nama_foto = $get_name['file_name'];
				
				$query = array(
			
					'name' 			=> $this->input->post('name'),
					'email'			=> $this->input->post('email'),
					'company'		=> $this->input->post('company'),
					'address'		=> $this->input->post('address'),
					'status'		=> 0,
					'pic'			=> $nama_foto,
					'testimonial'		=> $this->input->post('testimonial')
				);	
					
				$this->createThumbnail($nama_foto);
				$this->testimonial_m->insert_('testimonial', $query);			
				redirect(site_url().'/testimonial/front_end');
				
			}else{
							
						
				$query = array(
			
					'name' 			=> $this->input->post('name'),
					'email'			=> $this->input->post('email'),
					'company'		=> $this->input->post('company'),
					'address'		=> $this->input->post('address'),
					'status'		=> 0,
					'pic'			=> '',
					'testimonial'	=> $this->input->post('testimonial')
				);	
					
				$this->testimonial_m->insert_('testimonial', $query);			
				redirect(site_url().'/testimonial/front_end');
				
			}
					
		}else {
				
			redirect(site_url().'/testimonial/front_end');
			
		}		
	}

	function createThumbnail($fileName) {
			
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['image_library'] 	= 'gd2';
		$config['source_image'] 	= getcwd().'/uploads/testimonial/' . $fileName;
		$config['new_image'] 		= getcwd().'/uploads/testimonial/thumb/'. $fileName;
		$config['maintain_ratio'] 	= TRUE;
		$config['width'] 			= 110;
		$config['height'] 			= 110;
		
		$this->load->library('image_lib');
		$this->image_lib->clear();
		$this->image_lib->initialize($config);
			
		if(!$this->image_lib->resize()) { 
			echo $this->image_lib->display_errors();
			//var_dump($config);die();
		}
	}

	function _recaptcha_check($value) {
		
		$response = $this->recaptcha->recaptcha_check_answer ($_SERVER['REMOTE_ADDR'],  $this->input->post('recaptcha_challenge_field'),  		$this->input->post('recaptcha_response_field'));
		
		if(!$this->recaptcha->is_valid){
			//$this->recaptcha->error
			$this->form_validation->set_message('_recaptcha_check','Incorrect Recaptcha Value');
			return false;
		
		}else{
		
			return true;
		}
	}
}