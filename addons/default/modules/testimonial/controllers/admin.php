<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
class Admin extends Admin_Controller
{
	protected $section = 'testimonial';
	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model('testimonial_m');
		$this->load->helper('date');

	}
	
	public function index() {
		
		$title		= 'Testimonial';
		$total_rows = $this->testimonial_m->count_all_('testimonial');
		$pagination = create_pagination('admin/index', $total_rows);
		$order 		= $this->testimonial_m
					  ->limit($pagination['limit'], $pagination['offset'])->get_all_sort('testimonial', 'id_testimonial', 'DESC');
		
		$this->template
			 ->title($title)
			 ->set('judul', $title)
			 ->set('order', $order)
			 ->build('admin/testimonial/testimonial');		
	}
	
	function add() {
		
		$title			= "Add Testimonial";
		$form_action	= site_url().'/admin/testimonial/add_process'; 
		$status			= '';
		$pic 			= '';
		
		$this->template
			 ->title($title)
			 ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			 ->set('title', $title)
			 ->set('url', $form_action)
			 ->set('status', $status)
			 ->set('pic', $pic)
			 ->build('admin/testimonial/form');
		
	}
	
	function add_process() {
		$title			= "Add Testimonial";
		$form_action	= site_url().'/admin/testimonial/add_process'; 
		$status			= '';
		$pic 			= '';
		
		$nama_asli 					= $_FILES['userfile']['name'];	
		$judul 						= $this->input->post('name',TRUE);
		$config['file_name'] 		= $judul.'_'.'_'.$nama_asli;
		$config['upload_path'] 		= getcwd().'/uploads/testimonial/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size']			= '1024';
		$config['max_width']  		= '1600';
		$config['max_height']  		= '1200';
		
		$this->load->library('upload', $config);
		
		$this->form_validation->set_rules('name', 'Name', 'requires');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('company', 'Company', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('testimonial', 'Testimonial', 'required');
		$this->form_validation->set_rules('pic', 'pic', '');
		
		
		if ($this->form_validation->run() == TRUE) {
			
			if($this->upload->do_upload()){
					
				$get_name = $this->upload->data();
				$nama_foto = $get_name['file_name'];
				
				$query = array(
			
					'name' 			=> $this->input->post('name'),
					'email'			=> $this->input->post('email'),
					'company'		=> $this->input->post('company'),
					'address'		=> $this->input->post('address'),
					'status'		=> $this->input->post('status'),
					'pic'			=> $nama_foto,
					'testimonial'	=> $this->input->post('testimonial')
				);	
					
				$this->createThumbnail($nama_foto);
				$this->testimonial_m->insert_('testimonial', $query);			
				redirect(site_url().'/testimonial/admin');
					
			}else{
							
						
				$query = array(
			
					'name' 			=> $this->input->post('name'),
					'email'			=> $this->input->post('email'),
					'company'		=> $this->input->post('company'),
					'address'		=> $this->input->post('address'),
					'status'		=> $this->input->post('status'),
					'pic'			=> '',
					'testimonial'	=> $this->input->post('testimonial')
				);	
					
				$this->testimonial_m->insert_('testimonial', $query);			
				redirect(site_url().'/admin/testimonial');
			}
					
		}else {
				
			$this->template
			 	 ->title('Add Testimonial')
				 ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			  	 ->set('title', $title)
				 ->set('url', $form_action)
				 ->set('status', $status)
			 	 ->set('pic', $pic)
			 	 ->build('admin/testimonial/form');
		}		
	}

	function createThumbnail($fileName) {
			
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['image_library'] 	= 'gd2';
		$config['source_image'] 	= getcwd().'/uploads/testimonial/' . $fileName;
		$config['new_image'] 		= getcwd().'/uploads/testimonial/thumb/'. $fileName;
		$config['maintain_ratio'] 	= TRUE;
		$config['width'] 			= 110;
		$config['height'] 			= 110;
		
		$this->load->library('image_lib');
		$this->image_lib->clear();
		$this->image_lib->initialize($config);
			
		if(!$this->image_lib->resize()) { 
			echo $this->image_lib->display_errors();
			//var_dump($config);die();
		}
	}
	
	function detail($id_testimonial) {
		
		$title			= "Update testimonial";
		$form_action	= site_url().'/admin/testimonial/update_process';
		
		$testi			= $this->testimonial_m->get_by_id_('testimonial', 'id_testimonial', $id_testimonial);
		$this->session->set_userdata('id_testimonial', $testi->id_testimonial);
		
		//var_dump($testimonial); die();
		$name			= $testi->name;
		$email			= $testi->email;
		$company		= $testi->company;
		$address		= $testi->address;
		$testimonial	= $testi->testimonial;
		$pic			= $testi->pic;
		$status			= $testi->status;
		$tgl			= $testi->tgl;
		
		$this->template
			 	 ->title($title)
				 ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			  	 ->set('title', $title)
			 	 ->set('url', $form_action)
				 ->set('name', $name)
				 ->set('email', $email)
				 ->set('company', $company)
				 ->set('address', $address)
				 ->set('testimonial', $testimonial)
				 ->set('status', $status)
				 ->set('pic', $pic)
				 ->set('tgl', $tgl)
				 ->build('admin/testimonial/form');
	}

	function update_process() {
	
		$title			= "Update testimonial";
		$form_action	= site_url().'/admin/testimonial/update_process';
		
		$testi			= $this->testimonial_m->get_by_id_('testimonial', 'id_testimonial', $this->session->userdata('id_testimonial'));
		
		$name			= $testi->name;
		$email			= $testi->email;
		$company		= $testi->company;
		$address		= $testi->address;
		$testimonial	= $testi->testimonial;
		$pic			= $testi->pic;
		$status			= $testi->status;
		$tgl			= $testi->tgl;
		
		$nama_asli 					= $_FILES['userfile']['name'];	
		$judul 						= $this->input->post('name',TRUE);
		$config['file_name'] 		= $judul.'_'.'_'.$nama_asli;
		$config['upload_path'] 		= getcwd().'/uploads/testimonial/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size']			= '1024';
		$config['max_width']  		= '1600';
		$config['max_height']  		= '1200';
		
		$this->load->library('upload', $config);
		
		$this->form_validation->set_rules('name', 'Name', 'requires');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('company', 'Company', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('testimonial', 'Testimonial', 'required');
		$this->form_validation->set_rules('pic', 'pic', '');
		
		if ($this->form_validation->run() == TRUE) {
				
			if($this->upload->do_upload()){
					
				$get_name = $this->upload->data();
				$nama_foto = $get_name['file_name'];
				
				$query = array(
			
					'name' 			=> $this->input->post('name'),
					'email'			=> $this->input->post('email'),
					'company'		=> $this->input->post('company'),
					'address'		=> $this->input->post('address'),
					'status'		=> $this->input->post('status'),
					'pic'			=> $nama_foto,
					'testimonial'	=> $this->input->post('testimonial')
				);
				
				$this->createThumbnail($nama_foto);
				$this->testimonial_m->update_('id_testimonial', $this->session->userdata('id_testimonial'), 'testimonial', $query);
				redirect(site_url().'/admin/testimonial');
				
			}else{
						
				$query = array(
			
					'name' 			=> $this->input->post('name'),
					'email'			=> $this->input->post('email'),
					'company'		=> $this->input->post('company'),
					'address'		=> $this->input->post('address'),
					'status'		=> $this->input->post('status'),
					'pic'			=> $this->input->post('pic'),
					'testimonial'	=> $this->input->post('testimonial')
				);	
				
				$this->testimonial_m->update_('id_testimonial', $this->session->userdata('id_testimonial'), 'testimonial', $query);
				redirect(site_url().'/admin/testimonial');
			}
		
		}else {
			
			$this->template
			 	 ->title($title)
				 ->append_metadata($this->load->view('fragments/wysiwyg', array(), true))
			  	 ->set('title', $title)
			 	 ->set('url', $form_action)
				 ->set('name', $name)
				 ->set('email', $email)
				 ->set('company', $company)
				 ->set('address', $address)
				 ->set('testimonial', $testimonial)
				 ->set('status', $status)
				 ->set('pic', $pic)
				 ->set('tgl', $tgl)
				 ->build('admin/testimonial/form');
		}
	}
}