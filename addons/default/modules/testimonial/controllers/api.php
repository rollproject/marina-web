<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
class Api extends Public_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('testimonial_m');
		header('Access-Control-Allow-Origin: *');
	}

	public function index() {
	}
        
        public function get_testimonial() {
		$testi 		= $this->testimonial_m->get_testimonial();
		echo json_encode($testi);
	}
        
        public function upload_image(){
            $nama_file = $_FILES['userfile'];//['name']
            
            $nama_asli 				= $_FILES['userfile'];	//['name']
            $judul 				= $this->input->post('name',TRUE);
            $config['file_name'] 		= $judul.'_'.'_'.$nama_asli;
            $config['upload_path'] 		= getcwd().'/uploads/testimonial/';
            $config['allowed_types'] 	        = 'gif|jpg|png';
            $config['max_size']			= '1024';
            $config['max_width']  		= '1600';
            $config['max_height']  		= '1200';
            
            if($this->upload->do_upload()){
                $get_name = $this->upload->data();
                $nama_foto = $get_name['file_name'];
                
                /*$query = array(
        
                        'name' 			=> $this->input->post('name'),
                        'email'			=> $this->input->post('email'),
                        'company'		=> $this->input->post('company'),
                        'address'		=> $this->input->post('address'),
                        'status'		=> 0,
                        'pic'			=> $nama_foto,
                        'testimonial'		=> $this->input->post('testimonial')
                );*/	
                        
                $this->createThumbnail($nama_foto);
                //$this->testimonial_m->insert_('testimonial', $query);			
                //redirect(site_url().'/testimonial/front_end');
                echo $nama_foto;
            }else{
                echo 'salah';
            } 
        }
        
        function createThumbnail($fileName) {
			
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['image_library'] 	= 'gd2';
		$config['source_image'] 	= getcwd().'/uploads/testimonial/' . $fileName;
		$config['new_image'] 		= getcwd().'/uploads/testimonial/thumb/'. $fileName;
		$config['maintain_ratio'] 	= TRUE;
		$config['width'] 			= 110;
		$config['height'] 			= 110;
		
		$this->load->library('image_lib');
		$this->image_lib->clear();
		$this->image_lib->initialize($config);
			
		if(!$this->image_lib->resize()) { 
			echo $this->image_lib->display_errors();
			//var_dump($config);die();
		}
	}
        
        function get_testimonial_page(){
            $limit = $this->input->post('limit');
            $row   = 3;
            $testi = $this->testimonial_m->get_list_testimonial_limit($limit,$row);
	    echo json_encode($testi);
        }
        
        
        
        function add_testimonial(){
            
            $query = array(
			
                    'name' 			=> $this->input->post('name'),
                    'email'			=> $this->input->post('email'),
                    'company'		        => $this->input->post('company'),
                    'address'		        => $this->input->post('address'),
                    'status'		        => 0,
                    'pic'			=> '',
                    'testimonial'		=> $this->input->post('testimonial')
            );
            
            $this->testimonial_m->insert_mobile_('testimonial', $query);
            echo 'berhasil';
        }
        
        
}