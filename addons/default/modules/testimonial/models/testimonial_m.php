<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
class Testimonial_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
	}
	
	//global function
	function get_($order_by, $table) {
	
		$this->db->order_by($order_by);
		return $this->db->get($table)->result();
	}
	
	function delete_($table, $field_id, $id) {
		
		$this->db->delete($table, array($field_id => $id));
	}
	
	function insert_($table, $field) {
		
		$this->db->insert($table, $field);
	}
	
	function insert_mobile_($table, $field) {
		$this->db->set('tgl', 'NOW()', FALSE);
		$this->db->insert($table, $field);
	}
	
	function update_($id_field, $id, $table, $field) {
		
		$this->db->where($id_field, $id);
		$this->db->update($table, $field);
	}
	
	function get_all_($table, $field) {
					
		$this->db->select('*');
		$this->db->from($table);
		$this->db->order_by($field, 'ASC');
		return $this->db->get()->result();
	}
	
	function get_all_sort($table, $field, $val) {
					
		$this->db->select('*');
		$this->db->from($table);
		$this->db->order_by($field, $val);
		return $this->db->get()->result();
	}
	
	function get_by_id_($table, $field, $id) {
			
		return $this->db->get_where($table, array($field => $id))->row();
	}
	
	function count_all_($table) {
		
		return $this->db->count_all($table);
	}
	
	function get_testimonial() {
			
		$this->db->select('*');
		$this->db->order_by('id_testimonial', 'DESC');
		$result = $this->db->get_where('testimonial', array('status' => 1))->result();
		return $result;
			
	}
	
	function get_list_testimonial_limit($limit,$row){
		$this->db->select('*');
		$this->db->order_by('id_testimonial', 'DESC');
		$this->db->limit($row,$limit);
		$result = $this->db->get_where('testimonial', array('status' => 1))->result();
		return $result;
	}
}