<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a store module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-store
 * @subpackage 	Product
**/

// admin
//$route['testimonial/admin(/:any)?']			= 'testimonial$1';

//front end
//$route['product/category']					= 'front_end$1';
$route['testimonial/front_end(/:any)?']			= 'front_end$1';
