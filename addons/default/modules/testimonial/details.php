<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
class Module_Testimonial extends Module {

	public $version = '1.0.0';

	public function info()
	{
		return array(
			'name' => array(
			
				'en' => 'Testimonial',
			),
			
			'description' => array(
			
				'en' => 'Testimonial module with photo.',
			),
			'frontend'	=> TRUE,
			'backend'	=> TRUE,
			'skip_xss'	=> TRUE,
			'menu'		=> 'content',
			'author'	=> 'Max Solution',
		
			'roles'		=> array(
			
				'admin_testimonial'
			),
			
			'sections' => array(
			    'testimonial' => array(
				    'name'		=> 'Testomonial',
				    'uri'		=> 'admin/testimonial',
				    'shortcuts'	=> array(
						array(
					 	   'name'	=> 'Add',
						   'uri'	=> 'admin/testimonial/add',
						   'class'	=> 'add'
						),					
					)
				),
			)
		);
	}

	public function install() {

		$this->dbforge->drop_table('testimonial');
		
		$testimonial_fields = array(
			
			'id_testimonial'=> array('type' => 'INT', 'auto_increment' => true, 'primary' => true),
			'name' 			=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false),
			'email'			=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => true),
			'company' 		=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => true),
			'address'		=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => true),
			'testimonial'	=> array('type' => 'TEXT', 'null' => true),
			'status'		=> array('type' => 'INT', 'constraint' => 1, 'null' => true),
			'pic'			=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => true),
			'tgl'			=> array('type' => 'VARCHAR', 'constraint' => 10, 'null' => true),
		);
		
		return $this->install_tables(array(
			
			'testimonial'		=> $testimonial_fields, 
		));		
	}

	public function uninstall() {
		
		return TRUE;
	}


	public function upgrade($old_version) {
		
		TRUE;
	}
}
/* End of file details.php */