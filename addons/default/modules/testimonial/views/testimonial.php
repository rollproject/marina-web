<div id="middle">
	<div class="container">
		<div class="menu-content">
			<div class="page-title">{{ pages:display slug="testimonial" }} {{custom_fields}} {{page_title}} {{/custom_fields}} {{ /pages:display }} </div>
			<div id="page-banner-wrap">
				{{ pages:display slug="testimonial" }} {{custom_fields}} {{ image_banner:img }} {{/custom_fields}} {{ /pages:display }}
				<span class="color_1"></span>
				<span class="color_2"></span>
				<span class="color_3"></span>
				<span class="color_4"></span>				
				<div class="clear"></div>
			</div> <!-- #page-banner-wrap -->			
			<div class="page-description">
				{{ pages:display slug="testimonial" }} {{custom_fields}} {{body}} {{/custom_fields}} {{ /pages:display }}
				 <br />  <br />
                    <center>
                        <a class="button-auto orange" href="#">Post Comment</a>
                    </center>  
			</div>
			<div class="testimonial-form-wp">
				<?php echo form_open_multipart($url); ?>
					<div class="input-wp">
	                	<span>Your Name</span>
	                    <input type="text" name="name" />  
	                    <?php echo form_error('name'); ?>  
	               	</div>
	               	<div class="input-wp">
	                	<span>Your Email</span>
	                    <input type="text" name="email" />   
	                    <?php echo form_error('email'); ?> 
	               	</div>
	               	<div class="input-wp">
	                	<span>Your Company</span>
	                    <input type="text" name="company" /> 
	                    <?php echo form_error('company'); ?>   
	               	</div>
	               	<div class="input-wp">
	                	<span>Your Address</span>
	                    <input type="text" name="address" />  
	                    <?php echo form_error('address'); ?>  
	               	</div>
	               	<div class="input-wp">
	                	<span>Your Testimonial</span>
	                    <textarea rows="3" cols="5" name="testimonial"></textarea>    
	                    <?php echo form_error('testimonial'); ?>
	               	</div>
	               	<div class="input-wp">
	                	<span>Your Photo</span><br />
	                	<input type="file" name="userfile" value="" /><br />
	                    *) max size images 500x500    
	               	</div>
	               	<!-- Captcha HTML Code -->
	               	<div class="input-wp">
	                	<span></span><br />
					 	<?php echo form_error('recaptcha_challenge_field'); ?>	
						<div id="captcha-wrap">
							<?php  echo $recaptcha_html; ?>	
						</div>
					</div>	
	               	<div class="button-wrapper">
	               		<button type="submit" name="submit" id="submit" class="button-auto orange">Send</button> &nbsp;&nbsp;
                		<a class="button-auto cream">Cancel</a>
               		</div>
				<?php echo form_close(); ?>
            </div>
			<div class="general-wp">
				<?php if(count($testimonial) > 0) { ?> 
					<ul class="testimonial-list">
					<?php 
						$count = 0;
						foreach ($testimonial as $value) {
					?>
						<li>
                            <div class="date-wp">
                                <div class="date">12 AGS</div>
                                <div class="year">2013</div>
                            </div>
                            <?php if($value->pic != '') { ?> 
                            	
                            	<div class="single-testimonial">
	                                <div class="left-side">
	                                   <img class="thumbs" src="<?php echo base_url();?>uploads/testimonial/thumb/<?php echo $value->pic; ?>">
	                                </div>
	                                <div class="right-side">
	                                    <span class="comment-title"><?php echo $value->name; ?></span> - <span class="user"><?php echo $value->company; ?></span>
	                                    <div class="comment"><?php echo $value->testimonial; ?></div>
	                                </div>
	                                <div class="clear"></div>
	                            </div>
                            
                            <?php } else { ?> 
                            	
                            	<div class="single-testimonial">
	                                <span class="comment-title"><?php echo $value->name; ?></span> - <span class="user"><?php echo $value->company; ?></span>
	                                <div class="comment"><?php echo $value->testimonial; ?></div>
	                                <div class="clear"></div>
	                            </div>
	                            	
                            <?php } ?>
                            
                        </li>						
						<?php } ?>
					</ul>
					<?php } else { ?>
						
					<?php } ?>
					 <div class="button-wrapper">
                        <a class="button orange">Previous</a> <a class="button orange">Next</a>
                    </div>
			</div>
		<!--
		<div class="menu-mobile-title">Our Services</div>
		{{ widgets:instance id="1"}}
		-->
		<div class="clear"></div>
	</div>
</div>
			