<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a product module for PyroCMS
 *
 * @author 		Max Solution
 * @website		#
 * @package 	pyrocms-product
 * @subpackage 	product Module
**/
?>

<section class="title">
	<h4><?php echo $judul; ?></h4> 
</section>
<section class="item">
	<div class="content">
	<table>
		<thead>
			<td>No</td>
            <td>Images</td>
			<td>Name</td>
			<td>Email</td>
			<td>Company</td>
			<td>Address</td>
			<td>Status</td>
            <td align="center">#</td>
		</thead>
		<?php if (isset($order)) : ?>
		<tbody>
			<?php 
				
				$no = 1;
				foreach ($order as $val) { 
			
			?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td>
						<?php if($val->pic == '') { ?>
							<img src="<?php echo base_url(); ?>uploads/testimonial.png" width="70" height="70">
						<?php } else { ?>
							<img src="<?php echo base_url(); ?>uploads/testimonial/thumb/<?php echo $val->pic; ?>" width="70" height="70">
						<?php } ?>	
					</td>
					<td><?php echo $val->name; ?></td>
					<td><?php echo $val->email; ?></td>
					<td><?php echo $val->company; ?></td>
                    <td><?php echo $val->address; ?></td>
					<td><?php 
						
						if($val->status == 0){
							
							echo 'Draf';
						
						}else {
							echo 'Publish'; 
						} ?>
					</td>
                    <td>	
						<a href="<?php echo site_url().'/admin/testimonial/detail/'.$val->id_testimonial; ?>" class="btn blue">Detail</a>
					</td>
				</tr>
			<?php } ?>	
		</tbody>
		<?php endif; ?>
	</table>
	</div>
</section>

