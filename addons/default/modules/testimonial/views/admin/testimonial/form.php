<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<section class="title">
    <h4><?php echo $title;?></h4>
</section>

<section class="item">
	<div class="content">
		<div class="form_inputs">
			<!--<form name="testimonial"  method="post" action="<?php echo $url; ?>">-->
			<?php echo form_open_multipart($url); ?>
				<ul>
					<li>
						<label>Name <span>*</span></label><br />
						<input type="text" name="name" size="80" value="<?php echo set_value('name', isset($name) ? $name : ''); ?>" />
						<br />
					</li>
					<li>
						<label>Email <span>*</span></label><br />
						<input type="text" name="email" size="80" value="<?php echo set_value('email', isset($email) ? $email : ''); ?>" />
						<br />
					</li>
					<li>
						<label>Address <span>*</span></label><br />
						<input type="text" name="address" size="80" value="<?php echo set_value('address', isset($address) ? $address : ''); ?>" />
						<br />
					</li>
					<li>
						<label>Company <span>*</span></label><br />
						<input type="text" name="company" size="80" value="<?php echo set_value('company', isset($company) ? $company : ''); ?>" />
						<br />
					</li>
					<li>
						<label>Testimonial </label><br />
						<?php
						echo form_textarea(array('id' => 'testimonial', 'name' => 'testimonial', 'rows' => 30, 'value' => isset($testimonial) ? $testimonial : '','class' => 'wysiwyg-advanced'));
                        ?>
						<br />
					</li> 
                     <li>
						<label>Status Item <span>*</span></label><br />
						 <select name="status">
							<option value="0" <?php echo $status == 0 ? 'selected="selected"':''; ?>>Draf</option>
							<option value="1" <?php echo $status == 1 ? 'selected="selected"':''; ?>>Publish</option>
						</select>
                        <br />
					</li>
					<li>
                    	<label>Thumbneil</label><br />
                    	<?php if($pic == '') { ?>
									
							<br />
							<img src="<?php echo base_url(); ?>uploads/thumbnail-widget.png">
							<br />
									
						<?php }	else { ?> 
									
							<br />
							<img src="<?php echo base_url(); ?>uploads/product/thumb/<?php echo $pic; ?>">
							<input type="hidden" name="pic" value="<?php echo $pic; ?>"
							<br /><br />
									
						<?php } ?>
						<input type="file" name="userfile" value="" />
						*) max size images 500x500<br />
                    </li>
				</ul>
				<div class="buttons">
					<button type="submit" name="submit" id="submit" class="btn blue">save</button>
			  		<button type="reset" class="btn blue">cencel</button>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</section>